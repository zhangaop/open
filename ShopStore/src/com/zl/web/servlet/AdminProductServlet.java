package com.zl.web.servlet;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;

import com.zl.domain.Category;
import com.zl.domain.PageModel;
import com.zl.domain.Product;
import com.zl.service.CategoryService;
import com.zl.service.ProductService;
import com.zl.service.serviceimpl.CategoryServiceImpl;
import com.zl.service.serviceimpl.ProductServiceImpl;
import com.zl.utils.UUIDUtils;
import com.zl.utils.UploadUtils;
import com.zl.web.base.BaseServlet;

/**
 * 后台商品管理srvlet
 * @author hp
 *
 */
public class AdminProductServlet extends BaseServlet {
	public String findAllProductsWithPage(HttpServletRequest request,HttpServletResponse response) throws SQLException {
		//获取当前页num
		String num = request.getParameter("num");
		Integer curnum = Integer.parseInt(num);
		//显示每页商品数量
		int pageSize = 10;
		//调用业务层
		ProductService service = new ProductServiceImpl();
		PageModel pagem = service.findByPage(curnum,pageSize);
		//展示数据
		request.setAttribute("page", pagem);
		return "admin/product/list.jsp"; 
	}
	//添加商品
	public String saveUI(HttpServletRequest request,HttpServletResponse response) throws SQLException {
		CategoryService service2 = new CategoryServiceImpl();
		List<Category> list = service2.findAllCategory();
		request.setAttribute("c",list);
		return "admin/product/add.jsp";
		
	}
	public String addProduct(HttpServletRequest request,HttpServletResponse response) {
		//存储表单数据
		Map<String, String> map = new HashMap<String,String>();
		//携带表单数据到业务层
		Product product = new Product();
		
		try {
			//利用req.getInputStream()获取图片数据
			DiskFileItemFactory fac = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(fac);
			//获取请求信息
			List<FileItem> list = upload.parseRequest(request);
			//遍历集合
			for (FileItem fileItem : list) {
				//判断是否是普通项
				if(fileItem.isFormField()) {
					//如果是普通项
					//将普通项上name的值作为键,将获取的内容作为值,放入到Map中
					map.put(fileItem.getFieldName(), fileItem.getString("utf-8"));
				}else {
					//如果获得的数据是上传项
					//获取原始文件名称
					String oldFilename = fileItem.getName();//获取文件名
					//获取要保存文件的名称
					String newFilename = UploadUtils.getUUIDName(oldFilename);
					//通过FileItem获取到输入流对象,通过输入流可以获取到图片二进制数据
					InputStream is = fileItem.getInputStream();
					//获取到当前项目下products/3下的真实路径
					//D:\tomcat\tomcat71_sz07\webapps\store_v5\products\3
					String relpath = getServletContext().getRealPath("/products/3/");
					String dir = UploadUtils.getDir(newFilename); // /f/e/d/c/4/9/8/4
					String path = relpath+dir;//D:\tomcat\tomcat71_sz07\webapps\store_v5\products\3/f/e/d/c/4/9/8/4
					//内存中声明一个目录
					File newDir= new File(path);
					if(!newDir.exists()) {
						newDir.mkdirs();
					}
					//在服务端创建一个空文件(后缀必须和上传到服务端的文件名后缀一致)
					File finalFile=new File(newDir,newFilename);
					if(!finalFile.exists()) {
						finalFile.createNewFile();
					}
					//建立和空文件对应的输出流
					OutputStream os = new FileOutputStream(finalFile);
					//将输入流中的数据刷到输出流中
					IOUtils.copy(is, os);
					//释放资源
					IOUtils.closeQuietly(is);
					IOUtils.closeQuietly(os);
					//向map集合存入键值对数据
					map.put("pimage","/products/3/"+dir+"/"+newFilename);
					
					
				}
			}
			//利用beanutils将map和product相匹配
			BeanUtils.populate(product, map);
			product.setPid(UUIDUtils.getId());
			product.setPdate(new Date());
			product.setPflag(0);
			//将product存到数据库重定向到查询全部商品
			ProductService service = new ProductServiceImpl();
			service.saveProduct(product);
			response.sendRedirect(request.getContextPath()+"/AdminProductServlet?method=findAllProductsWithPage&num=1");
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	
	
}
