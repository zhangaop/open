package com.zl.domain;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class SaleVisit {
    private String visit_id;//
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date visit_time;
    private String visit_addr;
    private String visit_detail;
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date visit_nexttime;

    //访问记录表和客户表和用户表有这一对多的关系,多的一方需要添加一的一方的对象
    //拜访记录关联的客户
    private Customer customer;
    //拜访记录关联的用户对象
    private User user;

    public String getVisit_id() {
        return visit_id;
    }

    public void setVisit_id(String visit_id) {
        this.visit_id = visit_id;
    }

    public Date getVisit_time() {
        return visit_time;
    }

    public void setVisit_time(Date visit_time) {
        this.visit_time = visit_time;
    }

    public String getVisit_addr() {
        return visit_addr;
    }

    public void setVisit_addr(String visit_addr) {
        this.visit_addr = visit_addr;
    }

    public String getVisit_detail() {
        return visit_detail;
    }

    public void setVisit_detail(String visit_detail) {
        this.visit_detail = visit_detail;
    }

    public Date getVisit_nexttime() {
        return visit_nexttime;
    }

    public void setVisit_nexttime(Date visit_nexttime) {
        this.visit_nexttime = visit_nexttime;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
