package com.zl.dao.daoimpl;

import com.zl.dao.CustomerDao;
import com.zl.domain.Customer;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;

import java.util.List;

public class CustomerDaoImpl extends BaseDaoImpl<Customer> implements CustomerDao {
    public CustomerDaoImpl() {
        super(Customer.class);
    }
    //    @Override
//    public void save(Customer customer) {
//        this.getHibernateTemplate().save(customer);
//    }
//
//    @Override
//    public Integer count(DetachedCriteria detachedCriteria) {
//        detachedCriteria.setProjection(Projections.rowCount());
//        List<Long> list = (List<Long>) this.getHibernateTemplate().findByCriteria(detachedCriteria);
//        if(list.size()>0){
//           return list.get(0).intValue();
//        }
//        return null;
//    }
//
//    @Override
//    public List<Customer> findByPage(DetachedCriteria detachedCriteria, Integer begin, Integer pageSize) {
//        detachedCriteria.setProjection(null);
//
//        return (List<Customer>) this.getHibernateTemplate().findByCriteria(detachedCriteria,begin,pageSize);
//    }
//
//    @Override
//    public Customer findById(Long cust_id) {
//        Customer customer = this.getHibernateTemplate().get(Customer.class,cust_id);
//        return customer;
//    }

//    @Override
//    public void update(Customer customer) {
//        this.getHibernateTemplate().update(customer);
//    }

//    @Override
//    public void delete(Customer customer) {
//        this.getHibernateTemplate().delete(customer);
//    }

//    @Override
//    public List<Customer> findAll() {
//        return (List<Customer>) this.getHibernateTemplate().find("from Customer");
//    }
}
