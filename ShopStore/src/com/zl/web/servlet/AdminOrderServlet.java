package com.zl.web.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import com.zl.domain.OrderItem;
import com.zl.domain.Orders;
import com.zl.service.OrderService;
import com.zl.service.serviceimpl.OrderServiceImpl;
import com.zl.web.base.BaseServlet;

import net.sf.json.JSONArray;
import sun.print.resources.serviceui;

/**
 * Servlet implementation class AdminOrderServlet
 */
public class AdminOrderServlet extends BaseServlet {
	
	public String  findOrder(HttpServletRequest request ,HttpServletResponse response) throws SQLException {
		//获取state数据
		String st = request.getParameter("state");
		List<Orders> list=null;
		OrderService service = new OrderServiceImpl();
		if(null==st) {
			list = service.AdfindAllOrders();
		}else {
			list=service.AdfindOrderByState(st);
		}
		//将获取到的orders集合存到request
		request.setAttribute("orders", list);
		
		return "admin/order/list.jsp";
		
	}
	//异步获取订单详情
	public String showDetail(HttpServletRequest request ,HttpServletResponse response) throws SQLException, IOException{
		// 服务器获取订单id,
		String oid = request.getParameter("oid");
		//查询该订单的所有订单项以及订单项下的商品信息,返回集合
		OrderService service = new OrderServiceImpl();
		Orders order=service.findOrderByOid(oid);
		//将返回的集合转换为JSON格式字符串,响应到客户端
		String jsonStr=JSONArray.fromObject(order.getList()).toString();
		//响应到客户端
		response.setContentType("application/json;charset=utf-8");
		response.getWriter().println(jsonStr);
		return null;
		
	}
	//updateOrderByOid&oid
	public String updateOrderByOid(HttpServletRequest request ,HttpServletResponse response) throws SQLException, IOException{
		//获取oid
		String oid = request.getParameter("oid");
		//通过oid查询订单
		OrderService service = new OrderServiceImpl();
		Orders order = service.findOrderByOid(oid);
		//改变订单状态
		order.setState(3);
		//更新数据
		service.updateOrder(order);
		//重定向到lift
		response.sendRedirect(request.getContextPath()+"/AdminOrderServlet?method=findOrder&state=3");
		return null;
	
		
		
	}
}
