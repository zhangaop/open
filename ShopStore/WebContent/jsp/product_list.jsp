﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>商品列表</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css" type="text/css" />
		<script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js" type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/js/bootstrap.min.js" type="text/javascript"></script>
		<!-- 引入自定义css文件 style.css -->
		<link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css"/>

		<style>
			body {
				margin-top: 20px;
				margin: 0 auto;
				width: 100%;
			}
			.carousel-inner .item img {
				width: 100%;
				height: 300px;
			}
		</style>
	</head>

	<body>
		
			<%@include file="/jsp/header.jsp" %>


		<div class="row" style="width:1210px;margin:0 auto;">
			<div class="col-md-12">
				<ol class="breadcrumb">
					<li><a href="${pageContext.request.contextPath}/ProductServlet?method=findByCid&cid=${cid}&currentPage=1">首页</a></li>
				</ol>
			</div>
			
			<c:if test="${empty pagebean.list }">
				<div class="col-md-2">
					<h1>暂无数据</h1>
				</div>
			</c:if>
			<c:if test="${not empty pagebean.list }">
				<c:forEach items="${pagebean.list}" var="p">
					<div class="col-md-2" style="height: 250px">
						<a href="${pageContext.request.contextPath}/ProductServlet?method=findById&pid=${p.pid }&cid=${cid}&currentPage=${pagebean.currentPage}">
							<img src="${pageContext.request.contextPath}/${p.pimage}" width="170" height="170" style="display: inline-block;">
						</a>
						<p><a href="${pageContext.request.contextPath}/ProductServlet?method=findById&pid=${p.pid }&cid=${cid}&currentPage=${pagebean.currentPage}" style='color:green'>${p.pname }</a></p>
						<p><font color="#FF0000">商城价：&yen;${p.shop_price }</font></p>
					</div>
				</c:forEach>
			</c:if>
			
		</div>

		<!--分页 -->
		<div style="width:380px;margin:0 auto;margin-top:50px;">
			<ul class="pagination pagination-lg" style="text-align:center; margin-top:10px;">
			<c:if test="${pagebean.currentPage==1 }"><li class="disabled"><a href="javascript:void(0);">首页</a></li></c:if>
			<c:if test="${pagebean.currentPage!=1 }"><li><a href="${pageContext.request.contextPath}/ProductServlet?method=findByCid&cid=${cid}&currentPage=1">首页</a></li></c:if>
			
			
    
					<!-- 上一页 -->
			<c:if test="${pagebean.currentPage==1 }">
				<li class="disabled">
					<a href="javascript:void(0);" aria-label="Previous">
						<span aria-hidden="true">&laquo;</span>
					</a>
				</li>
			</c:if>
			<c:if test="${pagebean.currentPage!=1 }">
				<li>
					<a href="${pageContext.request.contextPath}/ProductServlet?method=findByCid&cid=${cid}&currentPage=${pagebean.currentPage-1}" aria-label="Previous">
						<span aria-hidden="true">&laquo;</span>
					</a>
				</li>
			</c:if>
					
				<!--显示每一页  -->
				<c:forEach begin="1" end="${pagebean.totalPage }" var="page">
					<!-- 判断是否是当前页如果是当前页则没有连接-->
					<c:if test="${pagebean.currentPage==page}">
					<!-- 禁止点击 -->
						<li class="active disabled"><a href="javascript:void(0)">${page }</a></li>
					</c:if>
					<!-- 判断是不是是当前页如果不是当前页则有链接-->
					<c:if test="${pagebean.currentPage!=page }">
						<li class="active"><a href="${pageContext.request.contextPath}/ProductServlet?method=findByCid&cid=${cid}&currentPage=${page}">${page }</a></li>
					</c:if>
					
				</c:forEach>
				
				
			<!-- 下一页 -->
			<c:if test="${pagebean.currentPage==pagebean.totalPage||pagebean.totalPage==0 }">
				<li class="disabled "> 
					<a href="javascript:void(0);" aria-label="Next"> 
						<span aria-hidden="true">&raquo;</span>
					</a>
				</li>
			</c:if>
			<c:if test="${pagebean.currentPage!=pagebean.totalPage&&pagebean.totalPage!=0 }">
				<li>
					<a href="${pageContext.request.contextPath}/ProductServlet?method=findByCid&cid=${cid}&currentPage=${pagebean.currentPage+1}" aria-label="Next"> 
						<span aria-hidden="true">&raquo;</span>
					</a>
				</li>
			</c:if>
			
			
			<c:if test="${pagebean.currentPage==pagebean.totalPage||pagebean.totalPage==0 }"><li class="disabled"><a href="javascript:void(0);">尾页</a></li></c:if>
			<c:if test="${pagebean.currentPage!=pagebean.totalPage&&pagebean.totalPage!=0 }"><li><a href="${pageContext.request.contextPath}/ProductServlet?method=findByCid&cid=${cid}&currentPage=${pagebean.totalPage}">尾页</a></li></c:if>
	
	
			<!--跳转  -->
				
			</ul>
		</div>
		<!-- 分页结束=======================        -->

		<!--
       		商品浏览记录:
        -->
		<div style="width:1210px;margin:0 auto; padding: 0 9px;border: 1px solid #ddd;border-top: 2px solid #999;height: 246px;">

			<h4 style="width: 50%;float: left;font: 14px/30px " 微软雅黑 ";">浏览记录</h4>
			<div style="width: 50%;float: right;text-align: right;"><a href="">more</a></div>
			<div style="clear: both;"></div>

			<div style="overflow: hidden;">

				<ul style="list-style: none;">
				<c:forEach items="${historyProductList}" var="hp">
					<li style="width: 150px;height: 216;float: left;margin: 0 8px 0 0;padding: 0 18px 15px;text-align: center;">
					<img src="${pageContext.request.contextPath}/${hp.pimage}" width="130px" height="130px" />
					</li>
				</c:forEach>
				</ul>

			</div>
		</div>
		
		<%@include file="/jsp/footer.jsp" %>
	</body>

</html>