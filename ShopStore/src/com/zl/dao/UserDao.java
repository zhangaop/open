package com.zl.dao;

import java.sql.SQLException;

import com.zl.domain.User;

public interface UserDao {
	//用户注册存入用户信息
	void UserRegist(User user)throws SQLException;
	//邮箱激活,查找相应用户激活码,存在则激活返回用户信息,不存在code未null
	User findByCode(String code) throws SQLException;
	//激活成功后更新操作,state为1,code为null
	void update(User userExist)throws SQLException;
	//登录用户信息验证
	User login(String username, String password)throws SQLException;
	//ajax异步校验用户名
	User findByUsername(String username)throws SQLException;


}
