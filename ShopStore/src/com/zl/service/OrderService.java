package com.zl.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.zl.domain.Orders;
import com.zl.domain.PageModel;
import com.zl.domain.User;

public interface OrderService {
	/**
	 * 保存提交的订单
	 * @param orders
	 * @throws SQLException
	 */
	void submitOrder(Orders orders)throws SQLException;
	/**
	 * 查找订单信息
	 * @param uid
	 * @return
	 * @throws SQLException
	 */
	List<Orders> findAllOrders(String uid)throws SQLException;
	/**
	 * 封装的是多个订单项和该订单项中的商品的信息
	 * @param oid
	 * @return
	 * @throws SQLException
	 */
	List<Map<String, Object>> findAllOrderItemByOid(String oid)throws SQLException;
	/**
	 * 提交订单更新收货人信息
	 * @param orders
	 */
	void updateOrderAddress(Orders orders)throws SQLException;
	/**
	 * 更新付款信息
	 * @param r6_Order
	 * @throws SQLException
	 */
	void updateOrderState(String r6_Order)throws SQLException;
	/**
	 * 计算分页的对象
	 * @param user
	 * @param curNum
	 * @return
	 */
	PageModel findMyOrderWithPage(User user, int curNum)throws SQLException;
	/**
	 * 后台查询所有订单
	 * @return
	 */
	List<Orders> AdfindAllOrders()throws SQLException;
	/**
	 * 后台查询不同状态的所有订单
	 * @return
	 */
	List<Orders> AdfindOrderByState(String st)throws SQLException ;
	Orders findOrderByOid(String oid)throws SQLException;
	/**
	 * 更新订单状态信息
	 * @param order
	 * @throws SQLException
	 */
	void updateOrder(Orders order)throws SQLException;

}
