package com.zl.dao;

import com.zl.domain.User;

public interface UserDao extends BaseDao<User> {
    void regist(User user);

    User login(User user);
}
