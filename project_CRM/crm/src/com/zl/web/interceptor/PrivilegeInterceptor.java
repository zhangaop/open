package com.zl.web.interceptor;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.interceptor.MethodFilterInterceptor;
import com.zl.domain.User;
import org.apache.struts2.ServletActionContext;

public class PrivilegeInterceptor extends MethodFilterInterceptor {
    @Override
    protected String doIntercept(ActionInvocation actionInvocation) throws Exception {
        User existUser = (User) ServletActionContext.getRequest().getSession().getAttribute("userExist");
        if(existUser == null){
            //没有登录
            ActionSupport actionSupport = (ActionSupport) actionInvocation.getAction();
            actionSupport.addActionError("您没有登录!没有访问权限!");
            return actionSupport.LOGIN;
        }else{
            //已登录
            return actionInvocation.invoke();
        }
    }
}
