package com.zl.web.servlet;

import java.sql.SQLException;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.zl.domain.User;
import com.zl.service.UserService;
import com.zl.service.serviceimpl.UserServiceImpl;
import com.zl.utils.CookUtils;
import com.zl.utils.MailUtils;
import com.zl.utils.MyBeanUtils;
import com.zl.utils.UUIDUtils;
import com.zl.web.base.BaseServlet;


public class UserServlet extends BaseServlet {
	//页面跳转带regist
	public String RegistUI(HttpServletRequest request, HttpServletResponse response) {
		
		return "/jsp/register.jsp";
	}
	
	//跳转到用户登录界面
	public String loginUI(HttpServletRequest request,HttpServletResponse response ) {
		//获取记住用户名的cookie数据
				Cookie ck=CookUtils.getCookieByName("remUser", request.getCookies());
				if(null != ck) {
					//如果cookie不为空,则表示记住了用户名
					request.setAttribute("remName",ck.getValue().split("@")[0] );
					request.setAttribute("remPass",ck.getValue().split("@")[1] );
				}
		return "/jsp/login.jsp";
	}
	
	//用户注册
	public String UserRegist(HttpServletRequest request,HttpServletResponse response) {
		/**
		 * 接收表单参数
		 * 调用业务层注册功能
		 * 注册成功向用户邮箱发送邮件,跳转到提示页面
		 * 注册失败跳转到提示页面
		 */
		//接收参数,可以getparameter获取参数,但是太繁琐
			//通过Map集合一次获取全部请求数据
		Map<String, String[]> map = request.getParameterMap();
		//创建一个user对象
		User user = new User();
		//自动填充map集合里的数据对应到user对象
		MyBeanUtils.populate(user, map);
		//为用户的uid,state和code赋值
		user.setUid(UUIDUtils.getId());
		user.setState(0);
		user.setCode(UUIDUtils.getCode());
		System.out.println(user);
		
		//调用业务层注册功能
		UserService UserService = new UserServiceImpl();
		try {
			UserService.UserRegist(user);
			//注册成功发送邮件
			MailUtils.sendMail(user.getEmail(), user.getCode());
			//注册成功向用户邮箱发送邮件,跳转到提示页面
			request.setAttribute("smsg", "用户注册成功,请激活!");
		} catch (Exception e) {
			e.printStackTrace();
			//注册失败跳转到提示页面
			request.setAttribute("wmsg", "用户注册失败,请重新注册!");
			return "/jsp/info.jsp";
		}
		return "/jsp/info.jsp";
		
	}
	
	//邮件激活,返回到登录界面
	public String active(HttpServletRequest request,HttpServletResponse response ) {
	try {
		UserService service = new UserServiceImpl();
		String code = request.getParameter("code");
		//调用业务层
		User user = service.findByCode(code);
		//不等于空则用户激活,state=1,code=null;
		if(user != null) {
			//如果去激活设置状态state为1
			user.setState(1);
			//设置code激活码为null
			user.setCode("");
			//更新用户信息
			service.update(user);

			//如果激活则,设置属性激活成功
			request.setAttribute("msg", "激活成功,请登录");
			System.out.println("激活成功,请登录");
			//return "/jsp/login.jsp";
		}
		} catch (Exception e) {
			//失败
			e.printStackTrace();
			request.setAttribute("wmsg","激活失败,请稍后重试.");
			
			return "/jsp/info.jsp";
		}
	return "/jsp/info.jsp";
	}
	

	
	//用户登录操作
	public String userLogin(HttpServletRequest request,HttpServletResponse response ) {
		try {
			//获取提交的信息
			String username = request.getParameter("username");
			String password = request.getParameter("password");
			//调用业务层
			UserService service = new UserServiceImpl();
			//返回use类型的所有参数
			User user = service.login(username,password);
			//判断登录结果
			if(user == null) {
				//user为空即用户不存在,表示账号或者密码不正确
				request.setAttribute("msg", "用户名或密码错误!请重新登录!");
				return "/jsp/login.jsp";
			}
			if(user.getState() != 1) {
				request.setAttribute("msg", "您未激活!请激活后在登录!");
				return "/jsp/login.jsp";
			}
			
			
			
			//获取自动登录xx
			String autoLogin = request.getParameter("autoLogin");
			if("1".equals(autoLogin)) {
				//判断是否勾选
				//如果勾选,这些创建coolkie存储username和password
				Cookie cookie = new Cookie("autoLogin",user.getUsername()+"@"+user.getPassword());
				//设置登录时间和路径
				cookie.setMaxAge(60*60*60);
				
				cookie.setPath("/");
				//添加cookie到response
				response.addCookie(cookie);
				
			}else {
				//如果不为一则删除cookie
				Cookie cookie = new Cookie("autoLogin", "");
				//清空时间
				cookie.setMaxAge(0);
				cookie.setPath("/");
				response.addCookie(cookie);
			}
			
			
			//记住密码
			//remUser
			String remUser=request.getParameter("remenberID");
			if("1".equals(remUser)){
				//用户选中自动登录复选框
				Cookie ck=new Cookie("remUser",user.getUsername()+"@"+user.getPassword());
				ck.setPath("/");
				ck.setMaxAge(23423424);
				response.addCookie(ck);
			//	System.out.println("1111112");
			}else {
				//如果不为一则删除cookie
				Cookie cookie = new Cookie("remUser", "");
				//清空时间
				cookie.setMaxAge(0);
				cookie.setPath("/");
				response.addCookie(cookie);
			}
			
		
			
			//否则已激活用户登录跳转到index.jsp页面
			//创建session将用户信息存到session
			HttpSession session = request.getSession();
			session.setAttribute("user", user);
			//重定向
			response.sendRedirect(request.getContextPath()+"/jsp/index.jsp");
		} catch (Exception e) {
			e.printStackTrace();
			// 跳到一个页面 告诉失败
			request.setAttribute("msg", "登录失败...");
			return "/jsp/info.jsp";
		}
		return null;
		
	}
	
	//退出登录
	public String loginOut(HttpServletRequest request,HttpServletResponse response) throws Exception {
		//获取session的信息,并移除
		request.getSession().removeAttribute("user");
		//重定向到首页
		response.sendRedirect(request.getContextPath()+"/UserServlet?method=loginUI");
		return null;
	}
	
	/**
	 * Ajax的异步校验用户名
	 * @throws SQLException 
	 */
	public String checkUsername(HttpServletRequest request,HttpServletResponse response) {
		//接收文本框的值
		String username = request.getParameter("username");
		
		System.out.println(username);
		//调用业务层查询用户名是否存
		UserService service = new UserServiceImpl();
		try {
			User user = service.findByUsername(username);
			//判断,如果返回值不等于空,则返回2否则返回1
			if (user != null) {
				//用户名被使用
				response.getWriter().print("2");
			} else {
				//用户名没有使用
				response.getWriter().print("1");
			} 
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
	
	

}
