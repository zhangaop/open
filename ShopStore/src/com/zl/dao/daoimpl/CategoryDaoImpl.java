package com.zl.dao.daoimpl;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.zl.dao.CategoryDao;
import com.zl.domain.Category;
import com.zl.utils.JDBCUtils;

public class CategoryDaoImpl implements CategoryDao{
	/**
	 * 查询所有的商品分类
	 */
	@Override
	public List<Category> findAllCategory() throws SQLException {
		//获取数据库连接
		QueryRunner runner = new QueryRunner(JDBCUtils.getDataSource());
		//编写SQL语句
		String sql = "Select * from category";
		return runner.query(sql, new BeanListHandler<Category>(Category.class));
	}
	/**
	 * 添加商品分类
	 */
	@Override
	public void addCategory(Category c) throws SQLException {
		QueryRunner	queryRunner = new QueryRunner(JDBCUtils.getDataSource());	
		String sql = "insert into category values(?,?)";
		queryRunner.update(sql, c.getCid(),c.getCname());
	}
	/**
	 * 删除分类
	 */
	@Override
	public void delCats(String cid) throws SQLException {
		QueryRunner runner = new QueryRunner(JDBCUtils.getDataSource());
		//首先换掉商品的cid
		String sql = "update product set cid = null where cid = ?";
		runner.update(sql,cid);
		String del = "delete from category where cid=?";
		runner.update(del,cid);
	}
	//获取cid的分类项
	@Override
	public Category findCatBycid(String cid) throws SQLException {
		QueryRunner runner = new QueryRunner(JDBCUtils.getDataSource());
		String sql = "select * from category where cid =?";
		return runner.query(sql, new BeanHandler<Category>(Category.class) , cid);
	}
	//更新商品
	@Override
	public void update(Category c) throws SQLException {
		QueryRunner runner = new QueryRunner(JDBCUtils.getDataSource());
		String sql = "update category set cname=? where cid =?";
		runner.update(sql,c.getCname(),c.getCid());
	}

}
