<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>关于我们</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css" type="text/css" />
		<script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js" type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/js/bootstrap.min.js" type="text/javascript"></script>
		<!-- 引入自定义css文件 style.css -->
		<link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css"/>
	</head>

	<body>

		<div class="container">
			<%@include file="/jsp/header.jsp" %>
			<div class="container">
				<!-- 普通无跳转的信息 -->
				<a href="#"><h1>${smsg}</h1></a>						
				<!-- 跳转到登录页面 -->
				<a href="${pageContext.request.contextPath}/UserServlet?method=loginUI"><h1>${msg}</h1></a>						
				<!-- 跳转到注册页面 -->
				<a href="${pageContext.request.contextPath}/UserServlet?method=RegistUI"><h1>${wmsg}</h1></a>						
			</div>
			<%@include file="/jsp/footer.jsp" %>
		</div>
	</body>

</html>