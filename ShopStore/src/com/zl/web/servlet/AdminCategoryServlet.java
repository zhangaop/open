package com.zl.web.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.Highlighter.HighlightPainter;

import com.zl.domain.Category;
import com.zl.service.CategoryService;
import com.zl.service.serviceimpl.CategoryServiceImpl;
import com.zl.utils.MyBeanUtils;
import com.zl.utils.UUIDUtils;
import com.zl.web.base.BaseServlet;

public class AdminCategoryServlet extends BaseServlet{
	public String findAllCats(HttpServletRequest request,HttpServletResponse response) throws SQLException {
		//获取全部商品信息
		CategoryService service = new CategoryServiceImpl();
		List<Category> list=service.findAllCategory();
		//获取将商品信息存到域中
		request.setAttribute("list", list);
		//返回到开后台页面
		return "/admin/category/list.jsp";
	}
	
	public String addCategoryUI(HttpServletRequest request,HttpServletResponse response) {
		return "/admin/category/add.jsp";
	}
	public String addCategory(HttpServletRequest request,HttpServletResponse response) throws SQLException, IOException {
		//获取分类名称信息
		String cname = request.getParameter("cname");
		Category c = new  Category();
		c.setCid(UUIDUtils.getId());
		c.setCname(cname);
		CategoryService categoryService = new CategoryServiceImpl();
		categoryService.addCategory(c);
		//重定向到findAllCats
		response.sendRedirect(request.getContextPath()+"/AdminCategoryServlet?method=findAllCats");
		return null;
	}

	public String delCats(HttpServletRequest request,HttpServletResponse response) throws SQLException, IOException {
		String cid = request.getParameter("cid");
		CategoryService service = new CategoryServiceImpl();
		service.delCats(cid);
		response.sendRedirect(request.getContextPath()+"/AdminCategoryServlet?method=findAllCats");
		
		return null;
	}
	//通过cid发现分类
	public String editCatsUI(HttpServletRequest request,HttpServletResponse response) throws SQLException {
		String cid = request.getParameter("cid");
		CategoryService service = new CategoryServiceImpl();
		Category c = service.findCatBycid(cid);
		request.setAttribute("c",c);
		return "/admin/category/edit.jsp";
		
	}
	
	//编辑商品分类信息edit
	public String edit(HttpServletRequest request,HttpServletResponse response) throws SQLException, IOException{
		String cid = request.getParameter("cid");
		String cname = request.getParameter("cname");
		//将获取到的数据与对象相匹配
		Category c = MyBeanUtils.populate(Category.class, request.getParameterMap());
		CategoryService service = new CategoryServiceImpl();
		service.edit(c);
		response.sendRedirect(request.getContextPath()+"/AdminCategoryServlet?method=findAllCats");
		return null;
		
	}
}
