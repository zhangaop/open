package com.zl.domain;

public class OrderItem {
	//封装的是订单里某一个商品的订单明细
	//订单项id,订单号,商品id,数量,小计
	private String itemid;//订单项的id
	private int count;//订单的数量
	private double subtotal;//小计
	private Product product;//商品对象的信息
	private Orders orders;//订单id
	public String getItemid() {
		return itemid;
	}
	public void setItemid(String itemid) {
		this.itemid = itemid;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public double getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	
	public Orders getOrders() {
		return orders;
	}
	public void setOrders(Orders orders) {
		this.orders = orders;
	}
	public OrderItem(String itemid, int count, double subtotal, Product product, Orders oid) {
		super();
		this.itemid = itemid;
		this.count = count;
		this.subtotal = subtotal;
		this.product = product;
		this.orders = oid;
	}
	public OrderItem() {
	}
	
	
	
	
}
