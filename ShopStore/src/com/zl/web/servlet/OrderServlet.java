package com.zl.web.servlet;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;

import com.zl.dao.OrderDao;
import com.zl.dao.daoimpl.OrderDaoImpl;
import com.zl.domain.Cart;
import com.zl.domain.CartItem;
import com.zl.domain.OrderItem;
import com.zl.domain.Orders;
import com.zl.domain.PageModel;
import com.zl.domain.Product;
import com.zl.domain.User;
import com.zl.service.OrderService;
import com.zl.service.serviceimpl.OrderServiceImpl;
import com.zl.utils.PaymentUtil;
import com.zl.utils.UUIDUtils;
import com.zl.web.base.BaseServlet;

public class OrderServlet extends BaseServlet {

	

	
	/**
	 * 提交订单
	 * @return 
	 * @throws IOException 
	 * @throws SQLException 
	 */
	public String submitOrder(HttpServletRequest request,HttpServletResponse response) throws IOException, SQLException {
		//确认用户登录状态
		User user = (User) request.getSession().getAttribute("user");
		//创建订单对象,为订单对象赋值
		Orders orders = new Orders();
//		private String oid;//订单号
		String oid = UUIDUtils.getId();
		orders.setOid(oid);
//		private Date ordertime;//下单时间
		orders.setOrdertime(new Date());
//		private double total;//总金额
		/**
		 * 需获取session中的购物车
		 */
		Cart cart = (Cart) request.getSession().getAttribute("cart");
		orders.setTotal(cart.getTotal());
//		private int state;//支付状态.0-未支付,1-已支付
		
		orders.setState(1);//起始是,未支付
		
//		private String address;//收货地址
		//orders.setAddress();
		
//		private String name;//收货人
		String name = request.getParameter("name");
		orders.setName(user.getName());
//		private String telephone;//电话
		String tel = request.getParameter("telephone");
		orders.setTelephone(user.getTelephone());
		
//		private User user;//订单属于哪个用户
		orders.setUser(user);
		
		/**订单中有多少订单项
		 * private List<OrderItem> list = new ArrayList<OrderItem>();
		 * 将购物项存到订单项里
		 */
		//遍历购物项的同时,创建订单项

		for (CartItem cartItem : cart.getCartItems()) {
			//创建订单项
			OrderItem orderItem = new OrderItem();
			
			//private String itemid;//订单项的id
			
			orderItem.setItemid(UUIDUtils.getId());
		
			//private int count;//订单的数量
			
			orderItem.setCount(cartItem.getNum());
			
			//private double subtotal;//小计
			
			orderItem.setSubtotal(cartItem.getSubtotal());
			
			//private Product product;//商品对象的信息
			
			orderItem.setProduct(cartItem.getProduct());
			
			//private Orders oid;//订单id

			orderItem.setOrders(orders);
			
			//将该订单项添加到集合中
			orders.getList().add(orderItem);
		}
		//调用业务层功能:保存订单
		OrderService service = new OrderServiceImpl();
		service.submitOrder(orders); 
		//清空购物车
		cart.clearCart();
		//将订单放入request
		request.setAttribute("orders", orders);
		//response.sendRedirect(request.getContextPath()+"/jsp/order_info.jsp");
		//转发/jsp/order_info.jsp
		return "/jsp/order_info.jsp";
	}
		//OrderServlet?method=payOrder
		//支付订单,更新收货人数据
		public String payOrder(HttpServletRequest request,HttpServletResponse response) throws IOException {
			//更新收货人,通过map集合一次性获取数据,获取地址,姓名,telephone
			Map<String, String[]> parameter = request.getParameterMap();
			//创建orders对象
			Orders orders = new Orders();
			//将order与参数匹配
				try {
					BeanUtils.populate(orders, parameter);
				} catch (IllegalAccessException e1) {
					e1.printStackTrace();
				} catch (InvocationTargetException e1) {
					e1.printStackTrace();
				}
			//通过service调用方法updateOrderAddress()
			OrderService service = new OrderServiceImpl();
			try {
				service.updateOrderAddress(orders);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			
			//订单支付
			/**
			 * 获得银行的标签
			 * 判断属于哪个银行
			 * 接入一个接口,集成了大部分的银行接口
			 */
			// 获得 支付必须基本数据
			String orderid = request.getParameter("oid");
			//String money = orders.getTotal()+"";
			String money = "0.1";
			// 银行
			String pd_FrpId = request.getParameter("pd_FrpId");

			// 发给支付公司需要哪些数据
			String p0_Cmd = "Buy";
			//String p1_MerId = ResourceBundle.getBundle("merchantInfo").getString("p1_MerId");
			String p1_MerId = "10001126856";
			String p2_Order = orderid;
			String p3_Amt = money;
			String p4_Cur = "CNY";
			String p5_Pid = "";
			String p6_Pcat = "";
			String p7_Pdesc = "";
			// 支付成功回调地址 ---- 第三方支付公司会访问、用户访问
			// 第三方支付可以访问网址
			String p8_Url ="http://localhost:8080/ShopStore/CallbackServlet";
			String p9_SAF = "";
			String pa_MP = "";
			String pr_NeedResponse = "1";
			// 加密hmac 需要密钥
			String keyValue = "69cl522AV6q613Ii4W6u8K6XuW8vM1N6bFgyv769220IuYe9u37N4y7rI4Pl";
			String hmac = PaymentUtil.buildHmac(p0_Cmd, p1_MerId, p2_Order, p3_Amt,
					p4_Cur, p5_Pid, p6_Pcat, p7_Pdesc, p8_Url, p9_SAF, pa_MP,
					pd_FrpId, pr_NeedResponse, keyValue);
			
			
			String url = "https://www.yeepay.com/app-merchant-proxy/node?pd_FrpId="+pd_FrpId+
							"&p0_Cmd="+p0_Cmd+
							"&p1_MerId="+p1_MerId+
							"&p2_Order="+p2_Order+
							"&p3_Amt="+p3_Amt+
							"&p4_Cur="+p4_Cur+
							"&p5_Pid="+p5_Pid+
							"&p6_Pcat="+p6_Pcat+
							"&p7_Pdesc="+p7_Pdesc+
							"&p8_Url="+p8_Url+
							"&p9_SAF="+p9_SAF+
							"&pa_MP="+pa_MP+
							"&pr_NeedResponse="+pr_NeedResponse+
							"&hmac="+hmac;

			//重定向到第三方支付平台
			response.sendRedirect(url);
			
			
			return null;
			
			
		}
	
	//我的订模块
	public void MyOrder(HttpServletRequest request,HttpServletResponse response) throws IOException, SQLException {
		//确认用户登录状态
			try {
						User user = (User) request.getSession().getAttribute("user");
				if(user==null) {
					request.setAttribute("msg", "请登录后再提交订单");
					response.sendRedirect(request.getContextPath()+"/jsp/login.jsp");
					return;
				}
				
		  //通过uid查询该用户的所有订单信息
				OrderService service = new OrderServiceImpl();
				
				//获取num当前页
				int curNum=Integer.parseInt(request.getParameter("num"));
				//select * from order where uid=? limt?,?;
				//获取分页对象
				PageModel pm=service.findMyOrderWithPage(user,curNum);
				//将pagemodel存到request域中
				//request.setAttribute("page",pm );
				
				OrderDao dao = new OrderDaoImpl();
				//通过分页对象的参数和user对象获取分页订单对象
				List<Orders> orders = dao.findMyOrderWithPage(user,pm.getStartIndex(),pm.getPageSize());
				
				//调用service的方法返回order的list集合
				//List<Orders> orders = service.findAllOrders(user.getUid());
				if(orders!=null) {
					//由于order表里缺少orderItem,
					//因此遍历order位订单添加订单项
					for (Orders order : orders) {
						//获取order的oid
						String oid  = order.getOid();
						//通过oid调用service层的方法,
						//返回一个list<Map<String,object>>的对象
						/**
						 * 由于一个订单有多个订单项所以返回的订单项要以集合来存储
						 * 又因为一个订单项包含某一类货物的多个商品因此也要用结合储存处
						 * 用obj主要是orditem的数据类型多样
						 */
						//maplist封装的是多个订单项和该订单项中的商品的信息
						List<Map<String, Object>> maplist=service.findAllOrderItemByOid(oid);
						//查询用户所有订单的订单项
						//订单项中右商品信息,需要遍历去添加,,或者通过多表查询来直接获取
						for (Map<String, Object> map: maplist) {
						try {
									//将map中的数据取出存到orderitem中
							//创建orderitem对象
							OrderItem orderItem = new OrderItem();
							//orderItem.setCount(Integer.parseInt(map.get("count").toString()));
							//通过工具类beanutils匹配数据
							BeanUtils.populate(orderItem, map);
							//创建product对象,匹配map里对应的数据
							Product product = new Product();
							BeanUtils.populate(product,map);
							//将product添加到orderitem
							orderItem.setProduct(product);
							//将orderitem添加到orders
							order.getList().add(orderItem);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				}
				
				//将order添加到pagemodel中
				pm.setList(orders);
				request.setAttribute("page", pm);
				
				//将orders添加到request域
				request.setAttribute("orders", orders);;
				//请求转发到order_list.jsp
				try {
					request.getRequestDispatcher("/jsp/order_list.jsp").forward(request, response);
				} catch (ServletException e) {
					e.printStackTrace();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
	}
	
}
