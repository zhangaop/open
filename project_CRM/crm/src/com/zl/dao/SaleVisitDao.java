package com.zl.dao;

import com.zl.domain.SaleVisit;

public interface SaleVisitDao extends BaseDao<SaleVisit> {
}
