package com.zl.web.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.zl.domain.PageBean;
import com.zl.domain.SaleVisit;
import com.zl.service.SaleVisitService;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

public class SaleVisitAction extends ActionSupport implements ModelDriven<SaleVisit> {
    private SaleVisit saleVisit = new SaleVisit();
    @Override
    public SaleVisit getModel() {
        return saleVisit;
    }
    @Resource(name="saleVisitService")
    private SaleVisitService saleVisitService;
    public void setSaleVisitService(SaleVisitService saleVisitService) {
        this.saleVisitService = saleVisitService;
    }

    /**
     * 获取分页信息
     * 当前页
     * 每页显示记录数
     */
    private Integer currentPage=1;
    private Integer pageSize=3;

    public void setCurrentPage(Integer currentPage) {
        if(currentPage==null){
            currentPage=1;
        }
        this.currentPage = currentPage;
    }

    public void setPageSize(Integer pageSize) {
        if(pageSize==null){
            pageSize=3;
        }
        this.pageSize = pageSize;
    }

    /**
     * 添加条件查询条件
     */
    private Date visit_end_time;

    public Date getVisit_end_time() {
        return visit_end_time;
    }

    public void setVisit_end_time(Date visit_end_time) {
        this.visit_end_time = visit_end_time;
    }

    /**
     * 分页查询拜访客户的列表
     * @return
     */
    public String findAll(){
        System.out.println("findall = "  );
        //获取离线条件查询
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(SaleVisit.class);

        if(saleVisit.getVisit_time()!=null){
            detachedCriteria.add(Restrictions.ge("visit_time",saleVisit.getVisit_time()));
        }
        if(visit_end_time!=null){
            detachedCriteria.add(Restrictions.le("visit_time",visit_end_time));
        }
        //设置条件,调用service
        PageBean<SaleVisit> pageBean = saleVisitService.findByPage(detachedCriteria,currentPage,pageSize);
        //page数据存到值栈
        ActionContext.getContext().getValueStack().push(pageBean);
        return "findAll";
    }

    /**
     * 跳转到增加拜访客户页面
     * 异步加载客户和用户的数据
     * @return
     */
    public String saveUI(){
        System.out.println("UI = " );
        return "saveUI";
    }

    /**
     * 添加拜访客户保存操作
     * @return
     */
    public String save(){
        System.out.println("save执行了 ");
        saleVisitService.save(saleVisit);
        return "saveSuccess";
    }

    /**
     * 修改拜访客户
     * @return
     */
    public String edit(){
        saleVisit = saleVisitService.findById(saleVisit.getVisit_id());
        ActionContext.getContext().getValueStack().push(saleVisit);
        return "editSuccess";
    }

    /**
     * 更新拜访客户信息
     * @return
     */
    public String update(){
        saleVisitService.update(saleVisit);
        return "updateSuccess";
    }

    /**
     * 删除拜访客户
     * @return
     */
    public String delete(){
        saleVisitService.delete(saleVisit);
        return "deleteSuccess";
    }
}
