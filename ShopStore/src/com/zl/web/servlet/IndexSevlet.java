package com.zl.web.servlet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zl.domain.Category;
import com.zl.domain.Product;
import com.zl.service.CategoryService;
import com.zl.service.ProductService;
import com.zl.service.serviceimpl.CategoryServiceImpl;
import com.zl.service.serviceimpl.ProductServiceImpl;
import com.zl.web.base.BaseServlet;

/**
 * Servlet implementation class IndexSevlet
 */
public class IndexSevlet extends BaseServlet {
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		//调用业务层功能,获取全部分类信息,返回集合
		CategoryService service = new CategoryServiceImpl();
		List<Category> list = service.findAllCategory();
		//将返回的集合放入request
		request.setAttribute("AllCategory", list);
		
		
		//业务调用层查询最新商品,查询最热商品,返回2个集合
		ProductService service2 = new ProductServiceImpl();
		List<Product> list01 = service2.findByHots();
		List<Product> list02=service2.findByNews();
		
		//将两个集合放到request
		request.setAttribute("hots", list01);
		request.setAttribute("news", list02);
		
		//转发到真实的首页
		return "/jsp/index.jsp";
	}
	
	
}
