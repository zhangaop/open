package com.zl.web.filter;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zl.domain.User;
import com.zl.service.UserService;
import com.zl.service.serviceimpl.UserServiceImpl;
import com.zl.utils.CookUtils;

public class LoginFilter2 implements Filter {

	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {
		// 强制转换到httpServlet
try {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) resp;
	
		  //如果是登录页直接放行 
		String servletPath = request.getServletPath();
		if(servletPath.startsWith("/UserServlet")) {
			//获取提交方式method
			String method =request.getParameter("method");
			if("loginUI".equals(method)) { 
				//放行
				chain.doFilter(request,response); 
				//结束条件语句 
				return; 
		  }
		}

		
		//1.先判断session是否有user
		User user = (User) request.getSession().getAttribute("user");
		//如果有,放行
		if(user != null) {
			chain.doFilter(request, response);
		}else {
			//否则失效了
			//2.判断cookie
			//2.1找到自动登录cookie
			Cookie userCookie = CookUtils.getCookieByName("autoLogin", request.getCookies());
			//如果cookie没有,即第一次来
			if(userCookie == null) {
				//放行
				chain.doFilter(request, response);
				
			}else {
				//存在cookie
				String[] u = userCookie.getValue().split("@");
				String username = u[0];
				String password = u[1];
				//完成登录
				User user01 = null;
				UserService service = new UserServiceImpl();
				user01 = service.login(username, password);
				//存入登录信息到session域中,方便下一次未过期前还可以用
				request.getSession().setAttribute("user", user01);
				//放行
				chain.doFilter(request, response);
			}
			
		}
		

		} catch (Exception e) {
			e.printStackTrace();
			chain.doFilter(req, resp);
		}

	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub

	}
}
