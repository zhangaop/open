package com.zl.utils;

import java.io.InputStream;
import java.sql.SQLException;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.zl.dao.UserDao;
import com.zl.domain.User;

public class BeanFactory {
	//解析xml
	public static Object createObject(String name) {
		try {
			//通过传递过来的那么获取application.xml中的name对应的class的值
			//获取Document对象
			SAXReader reader = new SAXReader();//
			//如果获取到的xml文件的输入流(xml文件必须在src下)
			InputStream is = BeanFactory.class.getClassLoader().getResourceAsStream("application.xml");
			Document doc = reader.read(is);
			//通过doc对象来获取根节点beans
			Element rootElement = doc.getRootElement();
			//通过根节点下的所有子节点bean返回集合
			List<Element> list = rootElement.elements();
			//遍历集合,判断每个元素上的id是否和当前name的一致
			for (Element element : list) {
				//element相当于beans节点下的每个bean
				//获取到当前节点的id属性值
				//如果一致,获取到当前元素上class属性值
				String id = element.attributeValue("id");
				if(id.equals(name)) {
					//获取bean的class值
					String str = element.attributeValue("class");
					//通过反射创建对象并返回
					Class clazz = Class.forName(str);
					//利用class值通过反射创建对象返回
					return clazz.newInstance();
					
				}
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public static void main(String[] args)throws SQLException {
		UserDao dao = (UserDao) BeanFactory.createObject("UserDao");
		User uu= dao.login("12345","qq");
		System.out.println(uu);
	}
}
