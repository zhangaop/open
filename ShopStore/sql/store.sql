/*
Navicat MySQL Data Transfer

Source Server         : root
Source Server Version : 50527
Source Host           : localhost:3306
Source Database       : store

Target Server Type    : MYSQL
Target Server Version : 50527
File Encoding         : 65001

Date: 2019-09-22 16:14:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `cid` varchar(32) NOT NULL,
  `cname` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('1', '手机数码');
INSERT INTO `category` VALUES ('172934bd636d485c98fd2d3d9cccd409', '运动户外');
INSERT INTO `category` VALUES ('2', '电脑办公');
INSERT INTO `category` VALUES ('3', '家具家居');
INSERT INTO `category` VALUES ('4', '鞋靴箱包');
INSERT INTO `category` VALUES ('5', '图书音像');
INSERT INTO `category` VALUES ('6', '母婴孕婴');
INSERT INTO `category` VALUES ('afdba41a139b4320a74904485bdb7719', '汽车用品');
INSERT INTO `category` VALUES ('BF624A5E30944E37BA05C989654AC315', '电器');
INSERT INTO `category` VALUES ('DCBBE182238D4B688C5C9FCB44652B36', '甜品蛋糕');

-- ----------------------------
-- Table structure for orderitem
-- ----------------------------
DROP TABLE IF EXISTS `orderitem`;
CREATE TABLE `orderitem` (
  `itemid` varchar(32) NOT NULL COMMENT '订单明细id',
  `count` int(11) DEFAULT NULL COMMENT '购买数量',
  `subtotal` double DEFAULT NULL COMMENT '小计',
  `pid` varchar(32) DEFAULT NULL COMMENT '外键,购买商品的id',
  `oid` varchar(32) DEFAULT NULL COMMENT '外键,订单表所在的订单id',
  PRIMARY KEY (`itemid`),
  KEY `order_item_fk_0001` (`pid`),
  KEY `order_item_fk_0002` (`oid`),
  CONSTRAINT `order_item_fk_0001` FOREIGN KEY (`pid`) REFERENCES `product` (`pid`),
  CONSTRAINT `order_item_fk_0002` FOREIGN KEY (`oid`) REFERENCES `orders` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of orderitem
-- ----------------------------
INSERT INTO `orderitem` VALUES ('34A15AEBC4C9455A873E485668447A14', '1', '4499', '34', '38B68BD177484A05A29101CBB09D33E6');
INSERT INTO `orderitem` VALUES ('36B45BC744CA467A8965B5968FCEEFF2', '1', '2599', '10', 'AB97EA05272D4D9981D25B864AFF1EBB');
INSERT INTO `orderitem` VALUES ('69E8BD65FC67433AADFB4F8C708E58F0', '1', '2599', '10', 'BB1D678B323E4C968FAB8C6CB536C91E');
INSERT INTO `orderitem` VALUES ('742204B6A87647529772B7690ECCB641', '1', '2298', '11', '025C329A1C4648609BF4032577387FC1');
INSERT INTO `orderitem` VALUES ('86CF7C69A9884F9BBDC8145BA6A496C4', '1', '4499', '34', '510C93221FCB46F29716CE8A55AB74FB');
INSERT INTO `orderitem` VALUES ('8727EB25BF024FB1B95F75EA76F58099', '1', '2599', '10', 'E2CC84A8E9A546B9BD7FABE3FD1E719D');
INSERT INTO `orderitem` VALUES ('8BB19B89176149F4893390E5EEE1D0C5', '1', '2599', '10', '32D8917AF6874D2689E163CE7D710F82');
INSERT INTO `orderitem` VALUES ('A33F61A171AF46D1924CF134B0EC0779', '1', '2599', '10', '5269C177A2B14E4F8451D1410B5FD0B1');
INSERT INTO `orderitem` VALUES ('BDADEEADC3ED47C691FFE4644675D477', '1', '2299', '31', '9455BC2D4D9A471B81E8EB3D4F5B5DD1');
INSERT INTO `orderitem` VALUES ('CD32EEE30C20478CA5744088459BB435', '1', '4499', '34', '22F94A3A840444849202CFDEFC2F77A3');
INSERT INTO `orderitem` VALUES ('DAAB71F3BCB04C98A81DFCA0FB774CD9', '1', '3299', '37', '7D4C5DA6F6C0452995F04A09D66E99C7');
INSERT INTO `orderitem` VALUES ('DE117F2AE1BE4ECC96EC3190330A9A29', '1', '3499', '18', 'AF2B59CFF8AB49B2A6276C6B8E413E0B');
INSERT INTO `orderitem` VALUES ('E9BCF6D930BA4267B9BE2BC57E22BCF6', '1', '32', 'A2F7893CE8A34B0DA04DAECF11F65E45', '38B68BD177484A05A29101CBB09D33E6');
INSERT INTO `orderitem` VALUES ('EF4FB5C1727B4AD49F99C9C662CBECA5', '1', '1299', '1', '38B68BD177484A05A29101CBB09D33E6');

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `oid` varchar(32) NOT NULL COMMENT '订单id',
  `ordertime` datetime DEFAULT NULL COMMENT '下单时间',
  `total` double DEFAULT NULL COMMENT '总价',
  `state` int(11) DEFAULT NULL COMMENT '订单状态：1=未付款;2=已付款,未发货;3=已发货,没收货;4=收货,订单结束',
  `address` varchar(30) DEFAULT NULL COMMENT '收获地址',
  `name` varchar(20) DEFAULT NULL COMMENT '收获人',
  `telephone` varchar(20) DEFAULT NULL COMMENT '收货人电话',
  `uid` varchar(32) DEFAULT NULL COMMENT '外键,用户id',
  PRIMARY KEY (`oid`),
  KEY `order_fk_0001` (`uid`),
  CONSTRAINT `order_fk_0001` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES ('025C329A1C4648609BF4032577387FC1', '2019-07-26 13:33:33', '2298', '1', '', '张磊', '123456', '04F6CFF52F874AEF9B103419A96BEBF1');
INSERT INTO `orders` VALUES ('22F94A3A840444849202CFDEFC2F77A3', '2019-07-26 11:36:11', '4499', '1', '服个软', '张磊', '123456', '04F6CFF52F874AEF9B103419A96BEBF1');
INSERT INTO `orders` VALUES ('32D8917AF6874D2689E163CE7D710F82', '2019-07-25 17:44:21', '2599', '1', null, '张磊', '123456', '04F6CFF52F874AEF9B103419A96BEBF1');
INSERT INTO `orders` VALUES ('38B68BD177484A05A29101CBB09D33E6', '2019-08-01 16:07:41', '5830', '3', '铂金', '张磊', '123456', '04F6CFF52F874AEF9B103419A96BEBF1');
INSERT INTO `orders` VALUES ('510C93221FCB46F29716CE8A55AB74FB', '2019-07-26 12:19:57', '4499', '2', '分为', '张磊', '123456', '04F6CFF52F874AEF9B103419A96BEBF1');
INSERT INTO `orders` VALUES ('5269C177A2B14E4F8451D1410B5FD0B1', '2019-07-25 15:33:22', '2599', '2', null, '张磊', '123456', '04F6CFF52F874AEF9B103419A96BEBF1');
INSERT INTO `orders` VALUES ('7D4C5DA6F6C0452995F04A09D66E99C7', '2019-07-25 15:43:36', '3299', '3', null, '张磊', '123456', '04F6CFF52F874AEF9B103419A96BEBF1');
INSERT INTO `orders` VALUES ('9455BC2D4D9A471B81E8EB3D4F5B5DD1', '2019-07-26 12:42:49', '2299', '1', '大V', '张磊', '123456', '04F6CFF52F874AEF9B103419A96BEBF1');
INSERT INTO `orders` VALUES ('AB97EA05272D4D9981D25B864AFF1EBB', '2019-07-25 14:02:47', '2599', '3', null, '张磊', '123456', '04F6CFF52F874AEF9B103419A96BEBF1');
INSERT INTO `orders` VALUES ('AF2B59CFF8AB49B2A6276C6B8E413E0B', '2019-07-25 17:48:06', '3499', '3', '安徽科技学院', '张磊', '123456', '04F6CFF52F874AEF9B103419A96BEBF1');
INSERT INTO `orders` VALUES ('BB1D678B323E4C968FAB8C6CB536C91E', '2019-07-25 17:16:20', '2599', '3', null, '张磊', '123456', '04F6CFF52F874AEF9B103419A96BEBF1');
INSERT INTO `orders` VALUES ('E2CC84A8E9A546B9BD7FABE3FD1E719D', '2019-07-26 11:23:39', '2599', '3', '会话', '张磊', '123456', '04F6CFF52F874AEF9B103419A96BEBF1');

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `pid` varchar(32) NOT NULL COMMENT '商品id',
  `pname` varchar(50) DEFAULT NULL COMMENT '商品名',
  `market_price` double DEFAULT NULL COMMENT '商品市场价',
  `shop_price` double DEFAULT NULL COMMENT '商品商城价',
  `pimage` varchar(200) DEFAULT NULL COMMENT '商品图片的路径地址',
  `pdate` date DEFAULT NULL COMMENT '上架日期',
  `is_hot` int(11) DEFAULT NULL COMMENT '是否热销,0=不热销,1=热销',
  `pdesc` varchar(255) DEFAULT NULL COMMENT '商品的描述',
  `pflag` int(11) DEFAULT NULL COMMENT '商品标记.0=未下架(默认值),1=下架',
  `cid` varchar(32) DEFAULT NULL COMMENT '分类id,外键,category',
  PRIMARY KEY (`pid`),
  KEY `cid` (`cid`),
  CONSTRAINT `product_ibfk_1` FOREIGN KEY (`cid`) REFERENCES `category` (`cid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('1', '小米note m4', '1399', '1299', 'products/1/c_0001.jpg', '2015-11-02', '1', '小米 4c 标准版 全网通 白色 移动联通电信4G手机 双卡双待', '0', '1');
INSERT INTO `product` VALUES ('10', '华为 Ascend Mate7', '2699', '2599', 'products/1/c_0010.jpg', '2015-11-02', '1', '华为 Ascend Mate7 月光银 移动4G手机 双卡双待双通6英寸高清大屏，纤薄机身，智能超八核，按压式指纹识别！!选择下方“移动老用户4G飞享合约”，无需换号，还有话费每月返还！', '0', '1');
INSERT INTO `product` VALUES ('11', 'vivo X5Pro', '2399', '2298', 'products/1/c_0014.jpg', '2015-11-02', '1', '移动联通双4G手机 3G运存版 极光白【购机送蓝牙耳机+蓝牙自拍杆】新升级3G运行内存·双2.5D弧面玻璃·眼球识别技术', '0', '1');
INSERT INTO `product` VALUES ('12', '努比亚（nubia）My 布拉格', '1899', '1799', 'products/1/c_0013.jpg', '2015-11-02', '0', '努比亚（nubia）My 布拉格 银白 移动联通4G手机 双卡双待【嗨11，下单立减100】金属机身，快速充电！布拉格相机全新体验！', '0', '1');
INSERT INTO `product` VALUES ('13', '华为 麦芒4', '2599', '2499', 'products/1/c_0012.jpg', '2015-11-02', '1', '华为 麦芒4 晨曦金 全网通版4G手机 双卡双待金属机身 2.5D弧面屏 指纹解锁 光学防抖', '0', '1');
INSERT INTO `product` VALUES ('14', 'vivo X5M', '1899', '1799', 'products/1/c_0011.jpg', '2015-11-02', '0', 'vivo X5M 移动4G手机 双卡双待 香槟金【购机送蓝牙耳机+蓝牙自拍杆】5.0英寸大屏显示·八核双卡双待·Hi-Fi移动KTV', '0', '1');
INSERT INTO `product` VALUES ('15', 'Apple iPhone 6 (A1586)', '4399', '4288', 'products/1/c_0015.jpg', '2015-11-02', '1', 'Apple iPhone 6 (A1586) 16GB 金色 移动联通电信4G手机长期省才是真的省！点击购机送费版，月月送话费，月月享优惠，畅享4G网络，就在联通4G！', '0', '1');
INSERT INTO `product` VALUES ('16', '华为 HUAWEI Mate S 臻享版', '4200', '4087', 'products/1/c_0016.jpg', '2015-11-03', '0', '华为 HUAWEI Mate S 臻享版 手机 极昼金 移动联通双4G(高配)满星评价即返30元话费啦；买就送电源+清水套+创意手机支架；优雅弧屏，mate7升级版', '0', '1');
INSERT INTO `product` VALUES ('17', '索尼(SONY) E6533 Z3+', '4099', '3999', 'products/1/c_0017.jpg', '2015-11-02', '0', '索尼(SONY) E6533 Z3+ 双卡双4G手机 防水防尘 涧湖绿索尼z3专业防水 2070万像素 移动联通双4G', '0', '1');
INSERT INTO `product` VALUES ('18', 'HTC One M9+', '3599', '3499', 'products/1/c_0018.jpg', '2015-11-02', '0', 'HTC One M9+（M9pw） 金银汇 移动联通双4G手机5.2英寸，8核CPU，指纹识别，UltraPixel超像素前置相机+2000万/200万后置双镜头相机！降价特卖，惊喜不断！', '0', '1');
INSERT INTO `product` VALUES ('19', 'HTC Desire 826d 32G 臻珠白', '1599', '1469', 'products/1/c_0020.jpg', '2015-11-02', '1', '后置1300万+UltraPixel超像素前置摄像头+【双】前置扬声器+5.5英寸【1080p】大屏！', '0', '1');
INSERT INTO `product` VALUES ('2', '中兴 AXON', '2899', '2699', 'products/1/c_0002.jpg', '2015-11-05', '1', '中兴 AXON 天机 mini 压力屏版 B2015 华尔金 移动联通电信4G 双卡双待', '0', '1');
INSERT INTO `product` VALUES ('20', '小米 红米2A 增强版 白色', '649', '549', 'products/1/c_0019.jpg', '2015-11-02', '0', '新增至2GB 内存+16GB容量！4G双卡双待，联芯 4 核 1.5GHz 处理器！', '0', '1');
INSERT INTO `product` VALUES ('21', '魅族 魅蓝note2 16GB 白色', '1099', '999', 'products/1/c_0021.jpg', '2015-11-02', '0', '现货速抢，抢完即止！5.5英寸1080P分辨率屏幕，64位八核1.3GHz处理器，1300万像素摄像头，双色温双闪光灯！', '0', '1');
INSERT INTO `product` VALUES ('22', '三星 Galaxy S5 (G9008W) 闪耀白', '2099', '1999', 'products/1/c_0022.jpg', '2015-11-02', '1', '5.1英寸全高清炫丽屏，2.5GHz四核处理器，1600万像素', '0', '1');
INSERT INTO `product` VALUES ('23', 'sonim XP7700 4G手机', '1799', '1699', 'products/1/c_0023.jpg', '2015-11-09', '1', '三防智能手机 移动/联通双4G 安全 黑黄色 双4G美国军工IP69 30天长待机 3米防水防摔 北斗', '0', '1');
INSERT INTO `product` VALUES ('24', '努比亚（nubia）Z9精英版 金色', '3988', '3888', 'products/1/c_0024.jpg', '2015-11-02', '1', '移动联通电信4G手机 双卡双待真正的无边框！金色尊贵版！4GB+64GB大内存！', '0', '1');
INSERT INTO `product` VALUES ('25', 'Apple iPhone 6 Plus (A1524) 16GB 金色', '5188', '4988', 'products/1/c_0025.jpg', '2015-11-02', '0', 'Apple iPhone 6 Plus (A1524) 16GB 金色 移动联通电信4G手机 硬货 硬实力', '0', '1');
INSERT INTO `product` VALUES ('26', 'Apple iPhone 6s (A1700) 64G 玫瑰金色', '6388', '6088', 'products/1/c_0026.jpg', '2015-11-02', '0', 'Apple iPhone 6 Plus (A1524) 16GB 金色 移动联通电信4G手机 硬货 硬实力', '0', '1');
INSERT INTO `product` VALUES ('27', '三星 Galaxy Note5（N9200）32G版', '5588', '5388', 'products/1/c_0027.jpg', '2015-11-02', '0', '旗舰机型！5.7英寸大屏，4+32G内存！不一样的SPen更优化的浮窗指令！赠无线充电板！', '0', '1');
INSERT INTO `product` VALUES ('28', '三星 Galaxy S6 Edge+（G9280）32G版 铂光金', '5999', '5888', 'products/1/c_0028.jpg', '2015-11-02', '0', '赠移动电源+自拍杆+三星OTG金属U盘+无线充电器+透明保护壳', '0', '1');
INSERT INTO `product` VALUES ('29', 'LG G4（H818）陶瓷白 国际版', '3018', '2978', 'products/1/c_0029.jpg', '2015-11-02', '0', '李敏镐代言，F1.8大光圈1600万后置摄像头，5.5英寸2K屏，3G+32G内存，LG年度旗舰机！', '0', '1');
INSERT INTO `product` VALUES ('3', '华为荣耀6', '1599', '1499', 'products/1/c_0003.jpg', '2015-11-02', '0', '荣耀 6 (H60-L01) 3GB内存标准版 黑色 移动4G手机', '0', '1');
INSERT INTO `product` VALUES ('30', '微软(Microsoft) Lumia 640 LTE DS (RM-1113)', '1099', '999', 'products/1/c_0030.jpg', '2015-11-02', '0', '微软首款双网双卡双4G手机，5.0英寸高清大屏，双网双卡双4G！', '0', '1');
INSERT INTO `product` VALUES ('31', '宏碁（acer）ATC705-N50 台式电脑', '2399', '2299', 'products/1/c_0031.jpg', '2015-11-02', '0', '爆款直降，满千减百，品质宏碁，特惠来袭，何必苦等11.11，早买早便宜！', '0', '2');
INSERT INTO `product` VALUES ('32', 'Apple MacBook Air MJVE2CH/A 13.3英寸', '6788', '6688', 'products/1/c_0032.jpg', '2015-11-02', '0', '宽屏笔记本电脑 128GB 闪存', '0', '2');
INSERT INTO `product` VALUES ('33', '联想（ThinkPad） 轻薄系列E450C(20EH0001CD)', '4399', '4199', 'products/1/c_0033.jpg', '2015-11-02', '0', '联想（ThinkPad） 轻薄系列E450C(20EH0001CD)14英寸笔记本电脑(i5-4210U 4G 500G 2G独显 Win8.1)', '0', '2');
INSERT INTO `product` VALUES ('34', '联想（Lenovo）小新V3000经典版', '4599', '4499', 'products/1/c_0034.jpg', '2015-11-02', '0', '14英寸超薄笔记本电脑（i7-5500U 4G 500G+8G SSHD 2G独显 全高清屏）黑色满1000減100，狂减！火力全开，横扫3天！', '0', '2');
INSERT INTO `product` VALUES ('35', '华硕（ASUS）经典系列R557LI', '3799', '3699', 'products/1/c_0035.jpg', '2015-11-02', '0', '15.6英寸笔记本电脑（i5-5200U 4G 7200转500G 2G独显 D刻 蓝牙 Win8.1 黑色）', '0', '2');
INSERT INTO `product` VALUES ('36', '华硕（ASUS）X450J', '4599', '4399', 'products/1/c_0036.jpg', '2015-11-02', '0', '14英寸笔记本电脑 （i5-4200H 4G 1TB GT940M 2G独显 蓝牙4.0 D刻 Win8.1 黑色）', '0', '2');
INSERT INTO `product` VALUES ('37', '戴尔（DELL）灵越 飞匣3000系列', '3399', '3299', 'products/1/c_0037.jpg', '2015-11-03', '0', ' Ins14C-4528B 14英寸笔记本（i5-5200U 4G 500G GT820M 2G独显 Win8）黑', '0', '2');
INSERT INTO `product` VALUES ('38', '惠普(HP)WASD 暗影精灵', '5699', '5499', 'products/1/c_0038.jpg', '2015-11-02', '0', '15.6英寸游戏笔记本电脑(i5-6300HQ 4G 1TB+128G SSD GTX950M 4G独显 Win10)', '0', '2');
INSERT INTO `product` VALUES ('39', 'Apple 配备 Retina 显示屏的 MacBook', '11299', '10288', 'products/1/c_0039.jpg', '2015-11-02', '0', 'Pro MF840CH/A 13.3英寸宽屏笔记本电脑 256GB 闪存', '0', '2');
INSERT INTO `product` VALUES ('4', '联想 P1', '2199', '1999', 'products/1/c_0004.jpg', '2015-11-02', '0', '联想 P1 16G 伯爵金 移动联通4G手机充电5分钟，通话3小时！科技源于超越！品质源于沉淀！5000mAh大电池！高端商务佳配！', '0', '1');
INSERT INTO `product` VALUES ('40', '机械革命（MECHREVO）MR X6S-M', '6799', '6599', 'products/1/c_0040.jpg', '2015-11-02', '0', '15.6英寸游戏本(I7-4710MQ 8G 64GSSD+1T GTX960M 2G独显 IPS屏 WIN7)黑色', '0', '2');
INSERT INTO `product` VALUES ('41', '神舟（HASEE） 战神K660D-i7D2', '5699', '5499', 'products/1/c_0041.jpg', '2015-11-02', '0', '15.6英寸游戏本(i7-4710MQ 8G 1TB GTX960M 2G独显 1080P)黑色', '0', '2');
INSERT INTO `product` VALUES ('42', '微星（MSI）GE62 2QC-264XCN', '6199', '5999', 'products/1/c_0042.jpg', '2015-11-02', '0', '15.6英寸游戏笔记本电脑（i5-4210H 8G 1T GTX960MG DDR5 2G 背光键盘）黑色', '0', '2');
INSERT INTO `product` VALUES ('43', '雷神（ThundeRobot）G150S', '5699', '5499', 'products/1/c_0043.jpg', '2015-11-02', '0', '15.6英寸游戏本 ( i7-4710MQ 4G 500G GTX950M 2G独显 包无亮点全高清屏) 金', '0', '2');
INSERT INTO `product` VALUES ('44', '惠普（HP）轻薄系列 HP', '3199', '3099', 'products/1/c_0044.jpg', '2015-11-02', '0', '15-r239TX 15.6英寸笔记本电脑（i5-5200U 4G 500G GT820M 2G独显 win8.1）金属灰', '0', '2');
INSERT INTO `product` VALUES ('45', '未来人类（Terrans Force）T5', '10999', '9899', 'products/1/c_0045.jpg', '2015-11-02', '0', '15.6英寸游戏本（i7-5700HQ 16G 120G固态+1TB GTX970M 3G GDDR5独显）黑', '0', '2');
INSERT INTO `product` VALUES ('46', '戴尔（DELL）Vostro 3800-R6308 台式电脑', '3299', '3199', 'products/1/c_0046.jpg', '2015-11-02', '0', '（i3-4170 4G 500G DVD 三年上门服务 Win7）黑', '0', '2');
INSERT INTO `product` VALUES ('47', '联想（Lenovo）H3050 台式电脑', '5099', '4899', 'products/1/c_0047.jpg', '2015-11-11', '0', '（i5-4460 4G 500G GT720 1G独显 DVD 千兆网卡 Win10）23英寸', '0', '2');
INSERT INTO `product` VALUES ('48', 'Apple iPad mini 2 ME279CH/A', '2088', '1888', 'products/1/c_0048.jpg', '2015-11-02', '0', '（配备 Retina 显示屏 7.9英寸 16G WLAN 机型 银色）', '0', '2');
INSERT INTO `product` VALUES ('49', '小米（MI）7.9英寸平板', '1399', '1299', 'products/1/c_0049.jpg', '2015-11-02', '0', 'WIFI 64GB（NVIDIA Tegra K1 2.2GHz 2G 64G 2048*1536视网膜屏 800W）白色', '0', '2');
INSERT INTO `product` VALUES ('4E2CE2D978BA4EE3BD868BA7C6D4EE1F', '芒果千层', '20', '10', '/products/3//2/7/38F5858FCBA74CC4B2F98B76AFC12FA4.jpg', '2019-07-30', '1', '第三方的舒适度法规发生', '0', '3');
INSERT INTO `product` VALUES ('5', '摩托罗拉 moto x（x+1）', '1799', '1699', 'products/1/c_0005.jpg', '2015-11-01', '0', '摩托罗拉 moto x（x+1）(XT1085) 32GB 天然竹 全网通4G手机11月11天！MOTO X震撼特惠来袭！1699元！带你玩转黑科技！天然材质，原生流畅系统！', '0', '1');
INSERT INTO `product` VALUES ('50', 'Apple iPad Air 2 MGLW2CH/A', '2399', '2299', 'products/1/c_0050.jpg', '2015-11-12', '0', '（9.7英寸 16G WLAN 机型 银色）', '0', '2');
INSERT INTO `product` VALUES ('6', '魅族 MX5 16GB 银黑色', '1899', '1799', 'products/1/c_0006.jpg', '2015-11-02', '0', '魅族 MX5 16GB 银黑色 移动联通双4G手机 双卡双待送原厂钢化膜+保护壳+耳机！5.5英寸大屏幕，3G运行内存，2070万+500万像素摄像头！长期省才是真的省！', '0', '1');
INSERT INTO `product` VALUES ('7', '三星 Galaxy On7', '1499', '1398', 'products/1/c_0007.jpg', '2015-11-14', '0', '三星 Galaxy On7（G6000）昂小七 金色 全网通4G手机 双卡双待新品火爆抢购中！京东尊享千元良机！5.5英寸高清大屏！1300+500W像素！评价赢30元话费券！', '0', '1');
INSERT INTO `product` VALUES ('8', 'NUU NU5', '1288', '1190', 'products/1/c_0008.jpg', '2015-11-02', '0', 'NUU NU5 16GB 移动联通双4G智能手机 双卡双待 晒单有礼 晨光金香港品牌 2.5D弧度前后钢化玻璃 随机附赠手机套+钢化贴膜 晒单送移动电源+蓝牙耳机', '0', '1');
INSERT INTO `product` VALUES ('9', '乐视（Letv）乐1pro（X800）', '2399', '2299', 'products/1/c_0009.jpg', '2015-11-02', '0', '乐视（Letv）乐1pro（X800）64GB 金色 移动联通4G手机 双卡双待乐视生态UI+5.5英寸2K屏+高通8核处理器+4GB运行内存+64GB存储+1300万摄像头！', '0', '1');
INSERT INTO `product` VALUES ('A2F7893CE8A34B0DA04DAECF11F65E45', '飞', '133', '32', '/products/3//3/1/B89D550B456A4439B0B2101907EEABD2.jpg', '2019-08-01', '1', '超凡大师', '0', '172934bd636d485c98fd2d3d9cccd409');
INSERT INTO `product` VALUES ('D31F2C67AE9E415CAB1445E36E84BA03', '蛋糕1', '99', '88', '/products/3//8/5/2024F59A29CD483CA908668DC0439CAF.jpg', '2019-07-30', '1', '纷纷问是服务范围分为', '0', 'DCBBE182238D4B688C5C9FCB44652B36');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `uid` varchar(32) NOT NULL COMMENT '用户编号',
  `username` varchar(20) DEFAULT NULL COMMENT '用户名',
  `password` varchar(20) DEFAULT NULL COMMENT '密码',
  `name` varchar(20) DEFAULT NULL COMMENT '昵称',
  `email` varchar(30) DEFAULT NULL COMMENT '电子邮件',
  `telephone` varchar(20) DEFAULT NULL COMMENT '电话',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `sex` varchar(10) DEFAULT NULL COMMENT '性别',
  `state` int(11) DEFAULT '0' COMMENT '状态,1=激活,0=未激活',
  `code` varchar(64) DEFAULT NULL COMMENT '激活码',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('04F6CFF52F874AEF9B103419A96BEBF1', '1234', 'q', '张磊', 'tom@zl.com', '123456', '2019-07-19', '男', '1', '');
INSERT INTO `user` VALUES ('EE0889EE75E249AC8B8BAE92864A5B1B', '12345', 'qq', '张磊', 'tom@zl.com', '123456', '2019-07-19', '男', '0', '');
INSERT INTO `user` VALUES ('F368C2CE3ACB464DA73304F41BF7F4DF', '1234', 'q', '信息', '113@qq.com', '', '2019-07-18', '女', '0', 'CB891FD3BF1A47A182A74C1093DEDC88');
