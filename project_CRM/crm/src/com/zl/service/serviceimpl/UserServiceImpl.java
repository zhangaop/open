package com.zl.service.serviceimpl;

import com.zl.dao.UserDao;
import com.zl.domain.User;
import com.zl.service.UserService;
import com.zl.utils.MD5Utils;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public class UserServiceImpl implements UserService {
    private UserDao dao;

    public void setDao(UserDao dao) {
        this.dao = dao;
    }

    @Override
    public List<User> findAll() {
        return dao.findAll();
    }

    /**
     * 用户注册
     * @param user
     */
    @Override
    public void regist(User user) {
//        md5加密工具 加密密码
        String password = MD5Utils.md5(user.getUser_password());
        user.setUser_password(password);
//        设置状态1
        user.setUser_state("1");
        dao.regist(user);
    }

    @Override
    public User login(User user) {
        //对user对象密码进行加密
        user.setUser_password(MD5Utils.md5(user.getUser_password()));
        //调用dao对象
        return dao.login(user);
    }
}
