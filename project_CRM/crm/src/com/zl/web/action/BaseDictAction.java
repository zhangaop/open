package com.zl.web.action;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.zl.domain.BaseDict;
import com.zl.service.BaseDictService;
import net.sf.json.JSONArray;
import net.sf.json.JsonConfig;
import org.apache.struts2.ServletActionContext;

import java.io.IOException;
import java.util.List;

public class BaseDictAction extends ActionSupport implements ModelDriven<BaseDict> {
    private BaseDict baseDict = new BaseDict();
    @Override
    public BaseDict getModel() {
        return baseDict;
    }
    private BaseDictService baseDictService;

    public void setBaseDictService(BaseDictService baseDictService) {
        this.baseDictService = baseDictService;
    }

    public  String findByTypeCode() throws IOException {
        System.out.println("BaseDictAction的find方法被执行了!.....");
        System.out.println(baseDict.getDict_type_code());
//        调用service
        List<BaseDict> list = baseDictService.findByTypeCode(baseDict.getDict_type_code());
//        将获得的字典对象数据转换成json数据
        /**
         * JSONConfig:转json的配置对象
         * JSONArroy:将数组和list集合转成json
         * JSONObject:将对象和map集合转成json
         */
        JsonConfig jsonConfig = new JsonConfig();
        //排除不需要的数据
        jsonConfig.setExcludes(new String[] {"dict_enable,dict_memo,dict_sort"} );
        //转换
        JSONArray jsonArray = JSONArray.fromObject(list,jsonConfig);
        System.out.println(jsonArray.toString());
        //将JSON打印到页面
        //由于是响应到jsp页面,因此需要设置字符编码,防止乱码
        ServletActionContext.getResponse().setContentType("text/html;charset=utf-8");
        ServletActionContext.getResponse().getWriter().println(jsonArray.toString());
        return NONE;
    }
}
