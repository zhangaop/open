package com.zl.service.serviceimpl;

import com.zl.dao.SaleVisitDao;
import com.zl.domain.Customer;
import com.zl.domain.PageBean;
import com.zl.domain.SaleVisit;
import com.zl.service.SaleVisitService;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
@Transactional
public class SaleVisitServiceImpl implements SaleVisitService {
    @Resource(name = "saleVisitDao")
    private SaleVisitDao saleVisitDao;
    public void setSaleVisitDao(SaleVisitDao saleVisitDao) {
        this.saleVisitDao = saleVisitDao;
    }

    @Override
    public PageBean<SaleVisit> findByPage(DetachedCriteria detachedCriteria, Integer currentPage, Integer pageSize) {
        //设置pagebean的数据
        //创建pagebean对象
        PageBean<SaleVisit> pageBean = new PageBean<SaleVisit>();
        //封装当前页数
        pageBean.setCurrentPage(currentPage);
        //封装每页记录数
        pageBean.setPageSize(pageSize);
        //封装总记录
        // 调用dao
        Integer totalCount = saleVisitDao.count(detachedCriteria);
        pageBean.setTotalcount(totalCount);
        //封装总页数(总的记录数除以每页记录数)
        Double tc = totalCount.doubleValue();
        Double totalPage = Math.ceil(tc/pageSize);//ceil向上取整
        pageBean.setTotalPage(totalPage.intValue());
        //封装每页显示数据的集合,设置起始索引值begin=(currentpage-1)*pagesize
        Integer begin = (currentPage-1)*pageSize;
        List<SaleVisit> list = saleVisitDao.findByPage(detachedCriteria,begin,pageSize);
        pageBean.setList(list);
        //返回pagebean对象
        return pageBean;
    }

    @Override
    public void save(SaleVisit saleVisit) {
        saleVisitDao.save(saleVisit);
    }

    @Override
    public SaleVisit findById(String visit_id) {
        return saleVisitDao.findById(visit_id);
    }

    @Override
    public void update(SaleVisit saleVisit) {
        saleVisitDao.update(saleVisit);
    }

    @Override
    public void delete(SaleVisit saleVisit) {
        saleVisitDao.delete(saleVisit);
    }
}
