package com.zl.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Orders {
	private String oid;//订单号
	private Date ordertime;//下单时间
	private double total;//总金额
	private int state;//支付状态.0-未支付,1-已支付
	
	private String address;//收货地址
	private String name;//收货人
	private String telephone;//电话
	
	private User user;//订单属于哪个用户uid

	private List<OrderItem> list = new ArrayList<OrderItem>();//orderitem对象
	
	public String getOid() {
		return oid;
	}

	public List<OrderItem> getList() {
		return list;
	}

	public void setList(List<OrderItem> list) {
		this.list = list;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}

	public Date getOrdertime() {
		return ordertime;
	}

	public void setOrdertime(Date ordertime) {
		this.ordertime = ordertime;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
}

