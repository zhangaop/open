package com.zl.dao.daoimpl;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import com.zl.dao.ProductDao;
import com.zl.domain.Product;
import com.zl.utils.JDBCUtils;

public class ProductDaoImpl implements ProductDao {
	/**
	 * 添加商品信息
	 */
	@Override
	public void saveProduct(Product product) throws SQLException {
		QueryRunner runner = new QueryRunner(JDBCUtils.getDataSource());
		String sql = "insert into product values(?,?,?,?,?,?,?,?,?,?)";
		runner.update(sql,product.getPid(),product.getPname(),product.getMarket_price(),product.getShop_price(),product.getPimage(),product.getPdate(),
				product.getIs_hot(),product.getPdesc(),product.getPflag(),product.getCid());
	}
	//获取所有商品信息
	@Override
	public List<Product> getProduct(int StartIndex,int PageSize) throws SQLException {
		QueryRunner runner = new QueryRunner(JDBCUtils.getDataSource());
		String sql = "select * from product limit ?,?";
		return runner.query(sql, new BeanListHandler<Product>(Product.class),StartIndex,PageSize);
	}
	/**
	 * 获取商品总数
	 */
	@Override
	public int getCountProduct() throws SQLException {
		QueryRunner runner = new QueryRunner(JDBCUtils.getDataSource());
		String sql = "select count(*) from product";
		Long l = (long) runner.query(sql, new ScalarHandler());
		return l.intValue();
	}
	/**
	 * 查询最新商品
	 */
	@Override
	public List<Product> findByNews() throws SQLException {
		QueryRunner runner = new QueryRunner(JDBCUtils.getDataSource());
		String sql = "SELECT * FROM product WHERE pflag=0 ORDER BY pdate DESC LIMIT 0,9";
		return runner.query(sql, new BeanListHandler<Product>(Product.class));
	}
	/**
	 * 查询最热商品
	 */
	@Override
	public List<Product> findByHots() throws SQLException {
		QueryRunner runner = new QueryRunner(JDBCUtils.getDataSource());
		String sql = "SELECT * FROM product WHERE pflag=0 AND is_hot=1 ORDER BY pdate DESC LIMIT 0,9";
		return runner.query(sql, new BeanListHandler<Product>(Product.class));
	}
	/**
	 * 通过pid查询商品信息
	 */
	@Override
	public Product findById(String pid) throws SQLException {
		QueryRunner runner = new QueryRunner(JDBCUtils.getDataSource());
		String sql = "select * from product where pid=?";
		return runner.query(sql, new BeanHandler<Product>(Product.class), pid);
	}
	/**
	 * 查找当前商品分类总数
	 */
	@Override
	public int getCount(String cid) throws SQLException {
		QueryRunner runner = new QueryRunner(JDBCUtils.getDataSource());
		String sql = "select count(*) from product where cid=?";
		Long l = (long) runner.query(sql, new ScalarHandler(),cid);
		return l.intValue();
	}
	/**
	 * 当前页显示的数据,通过cid,起始索引,每页的条数来查找
	 */
	@Override
	public List<Product> findByCidPage(String cid, int index, int pageSize) throws SQLException {
		QueryRunner runner = new QueryRunner(JDBCUtils.getDataSource());
		String sql = "select * from product where cid=? limit ?,?";
		return runner.query(sql, new BeanListHandler<Product>(Product.class), cid,index,pageSize);
	}

}
