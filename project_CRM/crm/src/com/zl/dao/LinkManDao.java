package com.zl.dao;

import com.zl.domain.Customer;
import com.zl.domain.LinkMan;
import org.hibernate.criterion.DetachedCriteria;

import java.util.List;

public interface LinkManDao extends BaseDao<LinkMan> {
    /**
     * 获取所有联系人总数
     * @param detachedCriteria
     * @return
     */
//    Integer count(DetachedCriteria detachedCriteria);

    /**
     * 分页查询所有联系人
     * @param detachedCriteria
     * @param begin
     * @param pageSize
     * @return
     */
//    List<LinkMan> findByPage(DetachedCriteria detachedCriteria, Integer begin, Integer pageSize);

    /**
     * 保存联系人信息
     * @param linkMan
     */
//    void save(LinkMan linkMan);

    /**
     * 修改联系人
     * 通过id查找联系人
     * @param lkm_id
     * @return
     */
//    LinkMan findById(Long lkm_id);

    /**
     * 更新修改联系人
     * @param linkMan
     */
//    void update(LinkMan linkMan);

    /**
     * 删除联系人
     * @param linkMan
     */
//    void delete(LinkMan linkMan);
}
