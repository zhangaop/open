package com.zl.service;

import com.zl.domain.Customer;
import com.zl.domain.PageBean;
import org.hibernate.criterion.DetachedCriteria;

import java.util.List;

public interface CustomerService {
//    添加客户
    void save(Customer customer);

    /**
     * 分页查询客户信息
     * @param detachedCriteria
     * @param currentPage
     * @param pageSize
     * @return
     */
    PageBean<Customer> findByPage(DetachedCriteria detachedCriteria, Integer currentPage, Integer pageSize);

    /**
     * 通过id查询客户信息
     * @param cust_id
     * @return
     */
    Customer findById(Long cust_id);

    /**
     * 修改客户信息
     * @param customer
     */
    void update(Customer customer);

    /**
     * 删除客户
     * @param customer
     */
    void delete(Customer customer);

    /**
     * 联系人/拜访客户管理查询所有客户
     * @return
     */
    List<Customer> findAll();
}
