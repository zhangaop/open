package com.zl.dao.daoimpl;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.DateConverter;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import com.zl.dao.OrderDao;
import com.zl.domain.OrderItem;
import com.zl.domain.Orders;
import com.zl.domain.Product;
import com.zl.domain.User;
import com.zl.utils.JDBCUtils;

public class OrderDaoImpl implements OrderDao {
	//后台更新订单信息
	@Override
	public void updateOrder(Orders order) throws SQLException {
		String sql="update orders set state=? where oid=?";
		QueryRunner qr=new QueryRunner(JDBCUtils.getDataSource());
		qr.update(sql, order.getState(),order.getOid());
	}
	//后台查询订单详情
	@Override
	public Orders findOrderByOid(String oid) throws SQLException {
		String sql="select * from orders where oid= ?";
		QueryRunner qr=new QueryRunner(JDBCUtils.getDataSource());
		Orders order=qr.query(sql, new BeanHandler<Orders>(Orders.class),oid);
		
		//根据订单id查询订单下所有的订单项以及订单项对应的商品信息
		sql="select * from orderitem o, product p where o.pid=p.pid and oid=?";
		List<Map<String, Object>> list02 = qr.query(sql, new MapListHandler(),oid);
		//遍历list
		for (Map<String, Object> map : list02) {
			OrderItem orderItem=new OrderItem();
			Product product=new Product();
			// 由于BeanUtils将字符串"1992-3-3"向user对象的setBithday();方法传递参数有问题,手动向BeanUtils注册一个时间类型转换器
			// 1_创建时间类型的转换器
			DateConverter dt = new DateConverter();
			// 2_设置转换的格式
			dt.setPattern("yyyy-MM-dd");
			// 3_注册转换器
			ConvertUtils.register(dt, java.util.Date.class);
			
			//将map中属于orderItem的数据自动填充到orderItem对象上
			try {
				BeanUtils.populate(orderItem, map);
			} catch (IllegalAccessException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (InvocationTargetException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			//将map中属于product的数据自动填充到product对象上
			try {
				BeanUtils.populate(product, map);
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//让每个订单项和商品发生关联关系
			orderItem.setProduct(product);
			//将每个订单项存入订单下的集合中
			order.getList().add(orderItem);
		}
		return order;
	}
	//获取总订单信息
	@Override
	public List<Orders> AdfindAllOrders() throws SQLException {
		QueryRunner runner = new QueryRunner(JDBCUtils.getDataSource());
		String sql ="select * from orders";
		return runner.query(sql, new BeanListHandler<Orders>(Orders.class));
	}
	//获取总订单信息
	@Override
	public List<Orders> AdfindOrderByState(String st) throws SQLException {
		QueryRunner runner = new QueryRunner(JDBCUtils.getDataSource());
		String sql ="select * from orders where state=?";
		return runner.query(sql, new BeanListHandler<Orders>(Orders.class),st);
	}

	// 存储订单信息
	@Override
	public void addOrder(Orders orders) throws SQLException {
		QueryRunner runner = new QueryRunner();
		String sql = "insert into orders values(?,?,?,?,?,?,?,?)";
		Connection conn = JDBCUtils.getConnection();
		runner.update(conn, sql, orders.getOid(), orders.getOrdertime(), orders.getTotal(), orders.getState(),
				orders.getAddress(), orders.getName(), orders.getTelephone(), orders.getUser().getUid());
	}

	// 存储订单项信息
	@Override
	public void addOrderItem(Orders orders) throws SQLException {
		QueryRunner runner = new QueryRunner();
		String sql = "insert into orderitem values(?,?,?,?,?)";
		Connection conn = JDBCUtils.getConnection();
//		由于订单项有多个所以要循环多次存储
		List<OrderItem> list = orders.getList();
		for (OrderItem orderItem : list) {
			runner.update(conn, sql, orderItem.getItemid(), orderItem.getCount(), orderItem.getSubtotal(),
					orderItem.getProduct().getPid(), orderItem.getOrders().getOid());
		}
	}

	/**
	 * 查询用户所有订单信息
	 */
	@Override
	public List<Orders> findAllOrders(String uid) throws SQLException {
		QueryRunner runner = new QueryRunner(JDBCUtils.getDataSource());
		String sql = "select * from orders where uid=?";
		return runner.query(sql, new BeanListHandler<Orders>(Orders.class), uid);

	}

	/**
	 * 封装的是多个订单项和该订单项中的商品的信息 private String itemid;//订单项的id private int
	 * count;//订单的数量 private double subtotal;//小计 private Product product;//商品对象的信息
	 * private Orders orders;//订单id
	 */
	@Override
	public List<Map<String, Object>> findAllOrderItemByOid(String oid) throws SQLException {
		QueryRunner runner = new QueryRunner(JDBCUtils.getDataSource());
		String sql = "select i.count,i.subtotal,p.pimage,p.pname,p.shop_price from orderitem i,product p where i.pid=p.pid and i.oid=?";
		return runner.query(sql, new MapListHandler(), oid);
	}

	/**
	 * 更新收货人信息
	 */
	@Override
	public void updateOrderAddress(Orders orders) throws SQLException {
		QueryRunner runner = new QueryRunner(JDBCUtils.getDataSource());
		String sql = "update orders set address=? ,name=?,telephone=? where oid=? ";
		runner.update(sql, orders.getAddress(), orders.getName(), orders.getTelephone(), orders.getOid());
	}

	/**
	 * 改变支付状态
	 */
	@Override
	public void updateOrderState(String r6_Order) throws SQLException {
		QueryRunner runner = new QueryRunner(JDBCUtils.getDataSource());
		String sql = "update orders set state=? where oid=? ";
		runner.update(sql, 1, r6_Order);
	}

	/**
	 * 获取我的订单分页信息
	 */
	// 获取订单总页数
	@Override
	public int gettotalRecords(User user) throws SQLException {
		QueryRunner runner = new QueryRunner(JDBCUtils.getDataSource());
		String sql = "select count(*) from orders where uid=?";
		Long l = (Long)runner.query(sql, new ScalarHandler(), user.getUid());
		return l.intValue();
	}
	//分页订单
	@Override
	public List<Orders> findMyOrderWithPage(User user, int startIndex, int pageSize) throws SQLException {
		QueryRunner runner = new QueryRunner(JDBCUtils.getDataSource());
		String sql ="select * from orders where uid=? limit ?,? ";
		return runner.query(sql, new BeanListHandler<Orders>(Orders.class), user.getUid(),startIndex,pageSize);
	}

}
