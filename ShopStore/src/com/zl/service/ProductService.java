package com.zl.service;

import java.sql.SQLException;
import java.util.List;

import com.zl.domain.PageBean;
import com.zl.domain.PageModel;
import com.zl.domain.Product;

public interface ProductService {
	/**
	 * 查询最新
	 */
	List<Product> findByNews()throws SQLException;
	/**
	 * 查询最热
	 */
	List<Product> findByHots()throws SQLException;
	/**
	 * 通过pid查询商品信息
	 * 
	 * @param pid
	 * @return
	 */
	Product dinsById(String pid)throws SQLException;
	/**
	 * 通过cid,int currentPage, int pageSize查询分类商品,并分页
	 * @param cid
	 * @return
	 * @throws SQLException
	 */
	//PageBean findByCid(String cid)throws SQLException;
	PageBean findByCid(String cid, int currentPage, int pageSize) throws SQLException;
	/**
	 *分页 查询所有商品
	 * @param curnum
	 * @param pageSize 
	 * @return
	 */
	PageModel findByPage(Integer curnum, int pageSize)throws SQLException;
	//添加商品
	void saveProduct(Product product)throws SQLException;

}
