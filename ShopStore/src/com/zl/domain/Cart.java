package com.zl.domain;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class Cart {
//	i. 定义一个购物项集合的属性,用于维护所有的购物项,集合采用Map<String,Product>
//	1) map.key:商品的id
//	2) map.value:购物项的信息   
//	通过商品pid来获取集合value的值 
	private Map<String, CartItem> map = new LinkedHashMap<String, CartItem>();
	//定义购物车总的统计,添加,删除等操作不进行计算,直接获得时一并计算
	private double total;
	//提供购物项集合属性,方便获取数据
	/*
	 * public Map<String, CartItem> getMap(){ return map; }
	 */
	//相当于属性名为:cartItem
	public Collection<CartItem> getCartItems(){
		return map.values();
	}
	
	//计算获得总计
	public double getTotal() {
		total=0;
		for (Map.Entry<String, CartItem> entry : map.entrySet()) {
			CartItem cartItem = entry.getValue();
			total+=cartItem.getSubtotal();
		}
		return total;
	}
	
	//方法:添加商品到购物车
	public void addCart(Product product,int num) {
		if(product == null) {
			return;
		}
		//通过商品pid获取购物项
		CartItem cartItem =map.get(product.getPid());
		
		//第一次购买
		if(cartItem == null) {
			cartItem = new CartItem();
			cartItem.setProduct(product);
			cartItem.setNum(num);
			//添加到购物车
			map.put(product.getPid(), cartItem);
		}else {
			cartItem.setNum(cartItem.getNum()+num);
		}
		//计算总计,两个方法一样
		//total +=cartItem.getSubtotal();//同一个商品的累加
		//total += num * product.getShop_price();
	}
	
	//从购物车中移除购物项
	public void removeCart(String pid ) {
		//从map中移除选中的元素
//		CartItem cartItem = map.get(pid);
		CartItem cartItem = map.remove(pid);
		//将总计-移除购物项的小计
//		total-=cartItem.getSubtotal();
	}
	
	//清空购物车
	public void clearCart() {
		//将map集合清空
		map.clear();
		//将总计设置为0
//		total=0;
	}
	
}
