package com.zl.utils;

import sun.applet.Main;

import java.io.File;
import java.util.UUID;

/**
 * 解决目录下文件名重复的问题
 */
public class UploadUtils {
    //设置随机的文件名
    public static String getUuidFileName(String fileName){
//        获取.号在文件名中的位置
        int idx = fileName.lastIndexOf(".");
//        获取扩展名
        String extions = fileName.substring(idx);
//        返回随机的文件名
        return UUID.randomUUID().toString().replace("-","")+extions;//replace去掉随机名称的横杠
    }

    /**
     * 目录分离的方法
     * @param uuidFileName
     * @return
     */
    public static String getPath(String uuidFileName){
        //获取
        int codel = uuidFileName.hashCode();
        int d1 = codel & 0xf;//作为一级目录
        int code2 = codel >>> 4;
        int d2 = code2 & 0xf;//作为2及目录
        return "/"+d1+"/"+d2;
    }

    public static void main(String[] args) {
        System.out.println(getUuidFileName("ss.txt"));
    }

}
