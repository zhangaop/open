package com.zl.web.servlet;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zl.domain.PageBean;
import com.zl.domain.Product;
import com.zl.service.ProductService;
import com.zl.service.serviceimpl.ProductServiceImpl;
import com.zl.web.base.BaseServlet;

public class ProductServlet<E> extends BaseServlet {
	private static final long serialVersionUID = 1L;
	/**
	 * 通过pid查询商品信息
	 * @param request
	 * @param response
	 * @return
	 * @throws SQLException
	 */
	public String findById(HttpServletRequest request,HttpServletResponse response) throws SQLException {
		/**
		 * 通过pid获取商品信息
		 */
		//获取传递的id
		String pid = request.getParameter("pid");
		//获取传递的cid和currentPage数据
		String cid = request.getParameter("cid");
		String currentPage = request.getParameter("currentPage");
		//调用service的方法
		ProductService service = new ProductServiceImpl();
		Product pro = service.dinsById(pid);
		//将获取到的数据存到requests域中
		request.setAttribute("product", pro);
		request.setAttribute("currentPage", currentPage);
		request.setAttribute("cid", cid);
		
		//获得用户携带的cookie信息,添加浏览记录
		String pids = pid;
		Cookie[] cookies = request.getCookies(); 
		if(cookies!=null) {
			for (Cookie cookie : cookies) {
				if("pids".equals(cookie.getName())) {
					pids = cookie.getValue();
					//获取pid的值存到string[]并转成linklist集合
					String[] split = pids.split("-");
					List<String> asList = Arrays.asList(split);
					LinkedList<String> list = new LinkedList<String>(asList);
					//判断pid是否存在集合中
					if(list.contains(pid)) {
						//包含当前查看商品的pid
						//删除pid所对应的的值
						list.remove(pid);
						//在头部添加pid对应的值
						list.addFirst(pid);
					}else {
						//不包含当前商品的pid,直接将pid放到头上
						list.addFirst(pid);
					}
					//将集合的数据转换成连接的cookie格式1-2-2
					StringBuffer sb = new StringBuffer();
					for(int i=0; i<list.size()&&i<7;i++) {
						sb.append(list.get(i));
						sb.append("-");
					}
					//去掉后面的-
					pids=sb.substring(0, sb.length()-1);
				}
				
			}
		}
		Cookie cookie_pids = new Cookie("pids", pids);
		response.addCookie(cookie_pids);
		
		
		//返回到product_info.jsp
		return "/jsp/product_info.jsp";
	}
	/**
	 * 通过cid查询分页信息
	 * @param request
	 * @param response
	 * @return
	 * @throws SQLException 
	 */
	public String findByCid(HttpServletRequest request,HttpServletResponse response) throws SQLException {
		
		try {
			//获取传递的cid
			String cid = request.getParameter("cid");
			//获取当前页
			String currentPageStr = request.getParameter("currentPage");
			if(currentPageStr==null) {
				currentPageStr="1";
			}
			int currentPage=Integer.parseInt(currentPageStr);
			//每页总数量
			int pageSize=12;
			//调用service
			ProductService service = new ProductServiceImpl();
			PageBean pageBean = service.findByCid(cid,currentPage,pageSize);
			request.setAttribute("pagebean", pageBean);
			request.setAttribute("cid", cid);
			
			//定义一个记录历史商品信息的集合
			List<Product> historyProducts = new ArrayList<Product>();
			//获取客户端传递的cookie
			Cookie[] cookies = request.getCookies();
			
			if (cookies!=null) {
				for (Cookie cookie : cookies) {
					if("pids".equals(cookie.getName())) {
						//如果pids存在则获取pid的值
						String pids = cookie.getValue();
						String[] split = pids.split("-");
						for(String pid:split) {
							Product pro = service.dinsById(pid);
							historyProducts.add(pro);
						}
						
					}
				}
			}
			
			//将历史记录的集合放到域中
			request.setAttribute("historyProductList", historyProducts);
			
			return "/jsp/product_list.jsp";
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "查询失败...");
			return "/jsp/info.jsp";
		}
	}
	
	
	
	
}
