package com.zl.dao.daoimpl;

import com.zl.dao.SaleVisitDao;
import com.zl.domain.SaleVisit;

public class SaleVisitDaoImpl extends BaseDaoImpl<SaleVisit> implements SaleVisitDao {
    public SaleVisitDaoImpl() {
        super(SaleVisit.class);
    }
}
