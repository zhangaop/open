package com.zl.service.serviceimpl;

import java.sql.SQLException;

import com.zl.dao.UserDao;
import com.zl.dao.daoimpl.UserDaoImpl;
import com.zl.domain.User;
import com.zl.service.UserService;
import com.zl.utils.BeanFactory;

public class UserServiceImpl implements UserService {
	UserDao dao = (UserDao) BeanFactory.createObject("UserDao");
	/**
	 * 用户注册
	 */
	@Override
	public void UserRegist(User user)throws SQLException {
		dao.UserRegist(user);
	}
	
	/**
	 * 邮箱激活验证
	 */
	@Override
	public User findByCode(String code) throws SQLException {
		return dao.findByCode(code);
	}
	
	/**
	 * 注册完成,更新用户信息
	 */
	@Override
	public void update(User user) throws SQLException {
		dao.update(user);
	}
	
	/**
	 * 用户登录的用户名.密码验证
	 */
	@Override
	public User login(String username, String password) throws SQLException {
		return dao.login(username,password);
	}
	/**
	 * ajax异步校验用户名
	 */
	@Override
	public User findByUsername(String username) throws SQLException {
		return dao.findByUsername(username);
	}
}
