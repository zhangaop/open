package com.zl.service;

import java.sql.SQLException;
import java.util.List;

import com.zl.domain.Category;

public interface CategoryService {
	/**
	 * 查询所有的商品分类
	 */
	List<Category> findAllCategory()throws SQLException;

	void addCategory(Category c)throws SQLException;

	void delCats(String cid)throws SQLException;

	Category findCatBycid(String cid)throws SQLException;

	void edit(Category c)throws SQLException;

}
