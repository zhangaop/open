package com.zl.service;

import com.zl.domain.BaseDict;

import java.util.List;

public interface BaseDictService {
    /**
     * 获取字典数据
     * @param dict_type_code
     * @return
     */
    List<BaseDict> findByTypeCode(String dict_type_code);
}
