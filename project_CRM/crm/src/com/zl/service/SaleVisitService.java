package com.zl.service;

import com.zl.domain.PageBean;
import com.zl.domain.SaleVisit;
import org.hibernate.criterion.DetachedCriteria;

public interface SaleVisitService {
    /**
     * 分页查询拜访客户列表
     * @param detachedCriteria
     * @param currentPage
     * @param pageSize
     * @return
     */
    PageBean<SaleVisit> findByPage(DetachedCriteria detachedCriteria, Integer currentPage, Integer pageSize);

    /**
     * 保存拜访客户
     * @param saleVisit
     */
    void save(SaleVisit saleVisit);



    /**
     *通过id查找拜访客户
     * @param visit_id
     * @return
     */
    SaleVisit findById(String visit_id);

    /**
     * 更新拜访客户
     * @param saleVisit
     */
    void update(SaleVisit saleVisit);

    /**
     * 删除拜访客户
     * @param saleVisit
     */
    void delete(SaleVisit saleVisit);
}
