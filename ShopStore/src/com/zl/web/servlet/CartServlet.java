package com.zl.web.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.Highlighter.HighlightPainter;

import com.zl.dao.daoimpl.OrderDaoImpl;
import com.zl.domain.Cart;
import com.zl.domain.CartItem;
import com.zl.domain.OrderItem;
import com.zl.domain.Orders;
import com.zl.domain.Product;
import com.zl.domain.User;
import com.zl.service.OrderService;
import com.zl.service.ProductService;
import com.zl.service.serviceimpl.OrderServiceImpl;
import com.zl.service.serviceimpl.ProductServiceImpl;
import com.zl.utils.UUIDUtils;
import com.zl.web.base.BaseServlet;

public class CartServlet extends BaseServlet {
	//添加购物车
	public String addCartItemToCart(HttpServletRequest request,HttpServletResponse response) throws Exception{
		//从session中获取购物车
		Cart cart=(Cart)request.getSession().getAttribute("cart");
		if(null == cart) {
			//如果获取不到常见session对象,放到session中,
			cart = new Cart();
			request.getSession().setAttribute("cart", cart);
			
		}
			//如果获取到,使用即可
			
			//获取到商品id,数量
			String pid = request.getParameter("pid");
			int num = Integer.parseInt(request.getParameter("num"));
			//通过商品id查询到商品对象
			ProductService service = new ProductServiceImpl();
			Product product = service.dinsById(pid);
			//调用购物车的方法
			cart.addCart(product, num);
			//重定向到/jsp/cart.jsp
			response.sendRedirect(request.getContextPath()+"/jsp/cart.jsp");
			return null;
	}
	//移除购物项
	public String removeCart(HttpServletRequest request,HttpServletResponse response) throws IOException {
		//获取商品id
		String pid = request.getParameter("pid");
		//获取购物车
		Cart cart = (Cart) request.getSession().getAttribute("cart");
		//通过cart的方法来移除购物项
		cart.removeCart(pid);
		//返回到界面
		response.sendRedirect(request.getContextPath()+"/jsp/cart.jsp");
		return null;
	}
	
	//移除购物车
	public String clearCart(HttpServletRequest request,HttpServletResponse response) throws IOException {
		//获取购物车
		Cart cart=(Cart)request.getSession().getAttribute("cart");
//		使用购物车的方法
		cart.clearCart();
		//重定向
		response.sendRedirect(request.getContextPath()+"/jsp/cart.jsp");
		return null;
	}
	

}
