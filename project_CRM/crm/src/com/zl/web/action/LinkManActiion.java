package com.zl.web.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.zl.domain.Customer;
import com.zl.domain.LinkMan;
import com.zl.domain.PageBean;
import com.zl.service.CustomerService;
import com.zl.service.LinkManService;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class LinkManActiion extends ActionSupport implements ModelDriven<LinkMan> {
    private LinkMan linkMan = new LinkMan();
    private LinkManService linkManService;
    private CustomerService customerService;

    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    public void setLinkManService(LinkManService linkManService) {
        this.linkManService = linkManService;
    }

    @Override
    public LinkMan getModel() {
        return linkMan;
    }

    /**
     * 新增联系人跳转页面
     * @return
     */
    public String saveUI(){
        //查询所有客户信息,传到添加页面
        List<Customer> list = customerService.findAll();
        //将查询结果压到值栈
        ActionContext.getContext().getValueStack().set("list",list);
        return "saveUI";
    }

    /**
     * 获取分页信息
     * 当前页
     * 每页显示记录数
     */
    private Integer currentPage=1;
    private Integer pageSize=3;

    public void setCurrentPage(Integer currentPage) {
        if(currentPage==null){
            currentPage=1;
        }
        this.currentPage = currentPage;
    }

    public void setPageSize(Integer pageSize) {
        if(pageSize==null){
            pageSize=3;
        }
        this.pageSize = pageSize;
    }

    /**
     * 查询所有联系人
     * @return
     */
    public String findAll() {
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(LinkMan.class);

        //编写条件查询
        //按名称和性别,判断是否为空
        if(linkMan.getLkm_name()!=null&&!"".equals(linkMan.getLkm_name())){
            detachedCriteria.add(Restrictions.like("lkm_name",linkMan.getLkm_name(), MatchMode.ANYWHERE));
        }
        if(linkMan.getLkm_gender()!=null&&!"".equals(linkMan.getLkm_gender())){
            detachedCriteria.add(Restrictions.eq("lkm_gender",linkMan.getLkm_gender()));
        }

        //设置分页查询条件,调用业务层查询,获取联系人数据集合
        PageBean<LinkMan> pageBean = linkManService.findAll(detachedCriteria,currentPage,pageSize);
        //压栈,将查询的数据放到值栈中
        ActionContext.getContext().getValueStack().push(pageBean);
        return "findAll";
    }

    /**
     * 保存联系人信息
     * @return
     */
    public String save(){
        //获取上传的数据
        //调用service的save方法
        linkManService.save(linkMan);
        return "saveSuccess";
    }

    /**
     * 编辑联系人信息,跳转到编辑界面
     * @return
     */
    public String edit(){
        //查询所有客户信息,添加到值栈
        List<Customer> list = customerService.findAll();
        //将list对象带到页面上
        ActionContext.getContext().getValueStack().set("list",list);
        //通过lkm_id查询联系人信息
         linkMan = linkManService.findById(linkMan.getLkm_id());
         //将linkMan对象存到值栈,Struct标签存到值栈自动回显
        ActionContext.getContext().getValueStack().push(linkMan);
         return "editSuccess";
    }

    /**
     * 更新联系人信息
     * @return
     */
    public String update(){
        linkManService.update(linkMan);
        return "update";
    }

    /**
     * 删除联系人
     */
    public String delete(){
        linkManService.delete(linkMan);
        return "deleteSuccess";
    }


}
