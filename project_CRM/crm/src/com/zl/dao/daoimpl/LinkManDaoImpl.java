package com.zl.dao.daoimpl;

import com.zl.dao.LinkManDao;
import com.zl.domain.Customer;
import com.zl.domain.LinkMan;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.RowCountProjection;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;

import java.util.List;

public class LinkManDaoImpl extends BaseDaoImpl<LinkMan> implements LinkManDao {
    public LinkManDaoImpl() {
        super(LinkMan.class);
    }

//        @Override
//    public Integer count(DetachedCriteria detachedCriteria) {
//        detachedCriteria.setProjection(Projections.rowCount());
//        //通过detachedCriteria的findByCriteria方法来进行数据库操作
//        List<Long> list = (List<Long>) this.getHibernateTemplate().findByCriteria(detachedCriteria);
//        //判断list是否存在
//        if(list.size()>0){
//           return list.get(0).intValue();
//        }
//        return null;
//    }

//    @Override
//    public List<LinkMan> findByPage(DetachedCriteria detachedCriteria, Integer begin, Integer pageSize) {
//        //清楚projection里面存在的条件
//        detachedCriteria.setProjection(null);
//        return (List<LinkMan>) this.getHibernateTemplate().findByCriteria(detachedCriteria,begin,pageSize);
//    }

//    @Override
//    public void save(LinkMan linkMan) {
//        this.getHibernateTemplate().save(linkMan);
//    }

//    @Override
//    public LinkMan findById(Long lkm_id) {
//        return this.getHibernateTemplate().get(LinkMan.class,lkm_id);
//    }

//    @Override
//    public void update(LinkMan linkMan) {
//        this.getHibernateTemplate().update(linkMan);
//    }

//    @Override
//    public void delete(LinkMan linkMan) {
//        this.getHibernateTemplate().delete(linkMan);
//    }
}
