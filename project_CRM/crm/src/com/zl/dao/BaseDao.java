package com.zl.dao;

import org.hibernate.criterion.DetachedCriteria;

import java.io.Serializable;
import java.util.List;

/**
 * 通用的dao接口
 * @param <T>
 */
public interface BaseDao<T> {
    public void save(T t);
    public void update(T t);
    public void delete(T t);

    /**
     * 通过id查询数据
     * @param id
     * @return
     */
    public T findById(Serializable id);

    /**
     * 查询所有
     * @return
     */
    public List<T> findAll();

    /**
     * 查询总数量
     * @param detachedCriteria
     * @return
     */
    public Integer count(DetachedCriteria detachedCriteria);

    /**
     * 分页查询
     * @param detachedCriteria
     * @param begin
     * @param pageSize
     * @return
     */
    public List<T> findByPage(DetachedCriteria detachedCriteria,Integer begin,Integer pageSize);
}
