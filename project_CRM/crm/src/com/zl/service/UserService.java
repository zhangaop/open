package com.zl.service;

import com.zl.domain.User;

import java.util.List;

public interface UserService {
    List<User> findAll();
    void regist(User user);

    User login(User user);
}
