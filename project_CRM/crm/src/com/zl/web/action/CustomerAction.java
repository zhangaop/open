package com.zl.web.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.zl.domain.Customer;
import com.zl.domain.PageBean;
import com.zl.service.CustomerService;
import com.zl.service.UserService;
import com.zl.utils.UploadUtils;
import net.sf.json.JSONArray;
import net.sf.json.JsonConfig;
import org.apache.struts2.ServletActionContext;
import org.aspectj.util.FileUtil;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.omg.CORBA.StringHolder;

import java.awt.datatransfer.FlavorEvent;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class CustomerAction extends ActionSupport implements ModelDriven<Customer> {
    private Customer customer = new Customer();
    private CustomerService customerService;

    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Override
    public Customer getModel() {
        return customer;
    }

    private Integer currentPage=1;
    private Integer pageSize=3;

    public void setCurrentPage(Integer currentPage) {
        if(currentPage==null){
            currentPage=1;
        }
        this.currentPage = currentPage;
    }

    public void setPageSize(Integer pageSize) {
        if(pageSize==null){
            pageSize=3;
        }
        this.pageSize = pageSize;
    }


    //struct2上传需要在Action中提供三个属性
    private String uploadFileName;//上传文件的名称,前面是表单的name属性加FileName
    private String uploadContextType;//上传文件类型
    private File upload;//上传的文件
    private String rootPath = "E:/img/upload";

    public void setUploadFileName(String uploadFileName) {
        this.uploadFileName = uploadFileName;
    }

    public void setUploadContextType(String uploadContextType) {
        this.uploadContextType = uploadContextType;
    }

    public void setUpload(File upload) {
        this.upload = upload;
    }

    //跳转到添加用户页面
    public String saveUI(){
        return "save" +
                "UI";
    }
    /**
     * 保存客户信息
     * @return
     */
    public String save() throws IOException {
//        获取客户提交的参数
//        获取图片参数
        //图片上传
        if(upload!=null){
            //设置上传路径
            String path = "E:/img/upload";
            //一个目录下存放多个相同文件名:解决办法随机文件名
            String uuidFileName = UploadUtils.getUuidFileName(uploadFileName);
            //一个目录下存放的文件的数量过多,通过目录分离解决
            String realPath = UploadUtils.getPath(uuidFileName);
            //创建目录
            String url = path + realPath;
            File file = new File(url);
            if(!file.exists()){
//                文件路径不存在创建目录
                file.mkdirs();
            }
//            文件上传
            File dictFile = new File(url+"/"+uuidFileName);
            FileUtil.copyFile(upload,dictFile);
//            设置image的属性值
            customer.setCust_image(url+"/"+uuidFileName);
        }
//        调用service执行
        customerService.save(customer);
//        ServletActionContext.getResponse().getWriter().print("alert(保存成功!)");
//        返回sucess
        return "toList";
    }


    /**
     * 获取客户列表
     * @return
     */
    public String findAll(){
        //接收参数:分页参数
        //使用离线查询对象分页查询
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Customer.class);
        //设置查询条件
        //设置cust_name条件
        if(customer.getCust_name()!=null && !"".equals(customer.getCust_name())){
            detachedCriteria.add(Restrictions.like("cust_name","%"+customer.getCust_name()+"%"));
        }
        //设置字典条件
        if (customer.getBaseDictSource() != null) {
            if (customer.getBaseDictSource().getDict_id() != null
                    && !"".equals(customer.getBaseDictSource().getDict_id())) {
                detachedCriteria
                        .add(Restrictions.eq("baseDictSource.dict_id", customer.getBaseDictSource().getDict_id()));
            }

        }
        if (customer.getBaseDictLevel() != null) {
            if (customer.getBaseDictLevel().getDict_id() != null
                    && !"".equals(customer.getBaseDictLevel().getDict_id())) {
                detachedCriteria
                        .add(Restrictions.eq("baseDictLevel.dict_id", customer.getBaseDictLevel().getDict_id()));
            }
        }
        if (customer.getBaseDictIndustry() != null && customer.getBaseDictIndustry().getDict_id() != null) {
            if (customer.getBaseDictIndustry().getDict_id() != null
                    && !"".equals(customer.getBaseDictIndustry().getDict_id())) {
                detachedCriteria
                        .add(Restrictions.eq("baseDictIndustry.dict_id", customer.getBaseDictIndustry().getDict_id()));
            }
        }

        //调用业务层,返回PageBean对象
        PageBean<Customer> pageBean = customerService.findByPage(detachedCriteria,currentPage,pageSize);
        //存到值栈
        ActionContext.getContext().getValueStack().push(pageBean);
        return "findAll";
    }

    /**
     * 修改客户
     * @return
     */
    public String edit(){
        //根据id查询客户数据,跳转页面回显据
        //调用业务层查询客户
        customer = customerService.findById(customer.getCust_id());
        /**
         * 将customer传递到页面有两种方式:
         * 1.手动压栈
         *      ActionContext.getContext().getValueStack().push(customer);
         *      回显数据<s:property value="cust_name"/>
         * 2.模型驱动不用任何操作
         *      回显数据需要将model.cust_*
         */
        return "editSuccess";
    }

    /**
     * 编写客户修改的方法update
     * 修改图片
     * @return
     */
    public String update() throws IOException {
        //修改图片
        //获得原有的图片进行删除,同时上传新图片
        //1.判断上传文件是否为空
        if(upload!=null){
            //获得原有图片
            //删除原有图片
            String cust_image = customer.getCust_image();
            if(cust_image!=null && !"".equals(cust_image)){
                //根据image路径获取文件
                File file = new File(cust_image);
                //判断文件是否存在
                if(file.exists()){
                    //删除旧文件
                    file.delete();
                }
            }
            //上传图片
            String uuidFileName = UploadUtils.getUuidFileName(uploadFileName);//设置随机名称
            //设置根路经
            String path = rootPath;
            //设置随机路径
            String realPath = UploadUtils.getPath(uploadFileName);//通过上传文件名获取hashcode值
            //获取全部路径
            String url = path+realPath;
            //获取根路径下的文件
            File file = new File(url);
            //判断文件夹是否处在,创建文件夹
            if(!file.exists()){
                file.mkdirs();
            }
            //创建文件
            File destFile = new File(url+"/"+uuidFileName);
            //复制文件到目的文件夹
            FileUtil.copyFile(upload,destFile);
            //设置图片路径
            customer.setCust_image(path);
        }
        customerService.update(customer);
        return "updateSuccess";
    }

    /**
     * 删除用户
     * @return
     */
    public String delete(){
        //通过id获取客户信息
        customer = customerService.findById(customer.getCust_id());
        //删除图片
        String url = customer.getCust_image();
        //判断路径是否为空
        if(url!=null && !"".equals(url)){
            //不为空这获取路径下的文件
            File file = new File(url);
            //判断文件是否存在
            if(file.exists()){
                //存在则删除
                file.delete();
            }
        }
            customerService.delete(customer);
            return "deleteSuccess";
    }

    /**
     * 异步加载客户信息到拜访客户管理添加 界面
     * @return
     */
    public String findAllCustomer() throws IOException {
        List<Customer> list = customerService.findAll();
        //1.获取json的配置
        JsonConfig jsonConfig = new JsonConfig();
        //设置不需要转换的属性
        jsonConfig.setExcludes(new String[]{"linkMans","baseDictSource","baseDictLevel","baseDictIndustry"});
        //转换成json
        JSONArray jsonArray = JSONArray.fromObject(list,jsonConfig);
        //响应到页面,设置响应的字符集
        ServletActionContext.getResponse().setContentType("text/html;charset=UTF-8");
        ServletActionContext.getResponse().getWriter().println(jsonArray.toString());
        return NONE;
    }
}
