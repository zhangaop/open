package com.zl.dao.daoimpl;

import java.sql.SQLException;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;

import com.zl.dao.UserDao;
import com.zl.domain.User;
import com.zl.utils.JDBCUtils;

public class UserDaoImpl implements UserDao {
	//用户注册存入用户信息
	@Override
	public void UserRegist(User user) throws SQLException {
		//创建sql语句
		String sql = "insert into user values(?,?,?,?,?,?,?,?,?,?)";
		//连接数据库连接池,获取连接,
		QueryRunner runner = new QueryRunner(JDBCUtils.getDataSource());
		//填写参数
		Object[] param = {user.getUid(),user.getUsername(),user.getPassword(),user.getName(),user.getEmail(),user.getTelephone(),user.getBirthday(),user.getSex(),user.getState(),user.getCode()};
		//执行更新操作
		runner.update(sql, param);
	}
	
	//邮箱激活,查找相应用户激活码,存在则激活返回用户信息,不存在code未null
	@Override
	public User findByCode(String code)throws SQLException {
		//通过jdbcutils来连接访问数据库,处理结果集,将结果集存到Javabean中,即存到user对象里
		QueryRunner runner = new QueryRunner(JDBCUtils.getDataSource());
		String sql = "select * from user where code = ?";
		//执行sql语句,将结果存到user对象中去
		return runner.query(sql, new BeanHandler<User>(User.class),code);
	}
	//激活成功后更新操作,state为1,code为null
	@Override
	public void update(User user)throws SQLException {
		//通过jdbcutils来连接访问数据库,处理结果集,将结果集存到Javabean中,即存到user对象里
		QueryRunner runner = new QueryRunner(JDBCUtils.getDataSource());
		String sql = "update user set username=?,password=?,name=?,email=?,telephone=?,birthday=?,sex=?,state=?,code=? where uid=?";
		Object[] params = {user.getUsername(),user.getPassword(),user.getName(),user.getEmail(),user.getTelephone(),user.getBirthday(),user.getSex(),user.getState(),user.getCode(),user.getUid()};
		runner.update(sql, params);
	}

	@Override
	public User login(String username, String password) throws SQLException {
		//通过jdbcutils来连接访问数据库,处理结果集,将结果集存到Javabean中,即存到user对象里
		QueryRunner runner = new QueryRunner(JDBCUtils.getDataSource());
		String sql = "select * from user where username=? and password=?";
		
		return runner.query(sql, new BeanHandler<User>(User.class), username,password);
	}

	@Override
	public User findByUsername(String username) throws SQLException {
		
		//通过jdbcutils来连接访问数据库,处理结果集,将结果集存到Javabean中,即存到user对象里
		QueryRunner runner = new QueryRunner(JDBCUtils.getDataSource());
		String sql = "select * from user where username=?";
		return runner.query(sql, new BeanHandler<User>(User.class), username);

	}

}
