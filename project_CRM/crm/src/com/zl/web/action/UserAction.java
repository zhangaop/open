package com.zl.web.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.zl.domain.User;
import com.zl.service.UserService;
import net.sf.json.JSONArray;
import net.sf.json.JsonConfig;
import org.apache.struts2.ServletActionContext;

import java.io.IOException;
import java.util.List;

public class UserAction extends ActionSupport implements ModelDriven<User> {

    private User user = new User();

    @Override
    public User getModel() {
        return user;
    }

    private UserService service;
    public void setService(UserService service) {
        this.service = service;
    }

    public UserService getService() {
        return service;
    }
    /**
     * 用户注册
     */
    public String regist(){
        service.regist(user);
        return LOGIN;
    }

    /**
     * 用户登录
     */
    private String verify;

    public String getVerify() {
        return verify;
    }

    public void setVerify(String verify) {
        this.verify = verify;
    }

    public String login(){
        //模型驱动获取登录信息
        //调用service查询用户是否存在
            //业务层对密码加密
        User userExist = service.login(user);
        //比较验证码
        // 比较验证码
        String serverCode = (String) ActionContext.getContext().getSession().get("SESSION_SECURITY_CODE");
        if (!serverCode.equals(verify) || verify==null || verify == "") {
            // 登陆失败，保存错误信息
            this.addActionError("验证码错误！");
            return LOGIN;
        }
        if(userExist == null){
        //如果不出在添加错误信息返回登录页面
            this.addActionError("用户名或密码错误");
            return LOGIN;
        }else {
        //如果存在将用户信息添加到servlet域或者context的session域返回sucess
//            第一种方法将结果集存入到域中ServletActionContext.getRequest().setAttribute("userExist",userExist);
            ActionContext.getContext().getSession().put("userExist",userExist);
            return SUCCESS;
        }

    }

    /**
     * 获取所有用户
     * @return
     */
    public String findAllUser() throws IOException {
        List<User> list = service.findAll();
        //1.获取json的配置
//        JsonConfig jsonConfig = new JsonConfig();
        //设置不需要转换的属性
//        jsonConfig.setExcludes(new String[]{"linkMans","baseDictSource","baseDictLevel","baseDictIndustry"});
        //转换成json
        JSONArray jsonArray = JSONArray.fromObject(list);
        //响应到页面,设置响应的字符集
        ServletActionContext.getResponse().setContentType("text/html;charset=UTF-8");
        ServletActionContext.getResponse().getWriter().println(jsonArray.toString());
        return NONE;
    }

}
