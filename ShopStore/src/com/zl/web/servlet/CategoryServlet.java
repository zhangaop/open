package com.zl.web.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zl.domain.Category;
import com.zl.service.CategoryService;
import com.zl.service.serviceimpl.CategoryServiceImpl;
import com.zl.utils.JedisUtils;
import com.zl.web.base.BaseServlet;

import net.sf.json.JSONArray;
import redis.clients.jedis.Jedis;

/**
 * Servlet implementation class CategoryServlet
 */
public class CategoryServlet extends BaseServlet {
	//findAllCategory
	public String findAllCategory(HttpServletRequest request, HttpServletResponse response) throws Exception {
		//设置字符编码
		response.setContentType("application/json;charset=utf-8");
		//在redis中获取全部分类信息
		Jedis jedis = JedisUtils.getJedis();
		String jsonString=jedis.get("allCats");
		if(null==jsonString || "".equals(jsonString)) {
			System.out.println("缓存中没有数据,正在存入");
			//调用业务层获取全部分类
			CategoryService service = new CategoryServiceImpl();
			List<Category> list =  service.findAllCategory();
			//将全部分类转换成json格式,获取json格式的字符串
			String jsonStr = JSONArray.fromObject(list).toString();
			//将全部分类信息响应到客户端
			response.getWriter().print(jsonStr);
			//将数据放入redis
			jedis.set("allCats", jsonStr);
		}else {
			System.out.println("缓存中有数据!");
		}
		//将全部分类信息响应到客户端
		response.getWriter().print(jsonString);
		//关闭jedis
		JedisUtils.closeJedis(jedis);
		System.out.println("关闭缓存");
		return null;
	}

}
