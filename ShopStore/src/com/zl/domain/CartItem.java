package com.zl.domain;

public class CartItem {
	private Product product;//携带购物项三种参数(图片路径,商品名称,商品价格)
	private int num;//当前类别商品的数量
	private double subtotal;//购买商品的小计,计算获得:商品的单价*购买数量
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public double getSubtotal() {
		this.subtotal = num*product.getShop_price();
		return subtotal;
	}
	
	//由于小计是计算获得,所以不需要setter方法
	/*
	 * public void setSubtotal(double subtotal) { this.subtotal = subtotal; }
	 */
	
}
