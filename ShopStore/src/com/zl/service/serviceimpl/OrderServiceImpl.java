package com.zl.service.serviceimpl;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.zl.dao.OrderDao;
import com.zl.dao.daoimpl.OrderDaoImpl;
import com.zl.domain.Orders;
import com.zl.domain.PageModel;
import com.zl.domain.User;
import com.zl.service.OrderService;
import com.zl.utils.JDBCUtils;

public class OrderServiceImpl implements OrderService {
	/**
	 * 保存提交的订单
	 */
	OrderDao dao = new OrderDaoImpl();
	@Override
	public void submitOrder(Orders orders) throws SQLException {
		/**
		 * 开启事务,由于在订单中存在许多单一的订单项
		 * 因此必须保证所有订单项都能成功存入数据库,所以需要开启事务
		 */
		try {
			//开启事务
			JDBCUtils.startTransaction();
			//调用dao存储order的方法
			dao.addOrder(orders);
			//调用dao存储orderitem表数据的方法
			dao.addOrderItem(orders);
		} catch (Exception e) {
			try {
				JDBCUtils.rollbackAndClose();
			} catch (Exception e2) {
				// TODO: handle exception
				e2.printStackTrace();
			}
			e.printStackTrace();
		}finally {
			try {
				JDBCUtils.commitAndClose();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}
	
	/**
	 * 获取用户所有订单信息
	 */
	@Override
	public List<Orders> findAllOrders(String uid) {
		
		List<Orders> orderList = null;
		try {
			orderList = dao.findAllOrders(uid);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return orderList;
	}
	/**
	 * 封装的是多个订单项和该订单项中的商品的信息
	 */
	@Override
	public List<Map<String, Object>> findAllOrderItemByOid(String oid) {
		List<Map<String, Object>> mapList = null;
		try {
			mapList = dao.findAllOrderItemByOid(oid);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return mapList;
	}
	/**
	 * 更新收货人信息
	 */
	@Override
	public void updateOrderAddress(Orders orders) throws SQLException {
		dao.updateOrderAddress(orders);
	}
	/**
	 * 支付成功改变支付状态
	 */
	@Override
	public void updateOrderState(String r6_Order) throws SQLException {
		dao.updateOrderState(r6_Order);
	}
	/**
	 * 获取订单分页信息
	 */
	@Override
	public PageModel findMyOrderWithPage(User user, int curNum) throws SQLException {
		//1.创建pageModel对象,目的是计算并且携带分页参数
		int totalrecords=dao.gettotalRecords(user);
		//创建分页信息
		PageModel pModel=new PageModel(curNum, totalrecords, 3);
		//关联集合
		// List list = dao.findMyOrderWithPage(user,pModel.getStartIndex(),pModel.getPageSize());
		 //pModel.setList(list);
		//关联url
		pModel.setUrl("OrderServlet?method=MyOrder");
		return pModel;
	}
	//后台查询所有订单
	@Override
	public List<Orders> AdfindAllOrders() throws SQLException {
		return dao.AdfindAllOrders();
	}
	//后台查询所有订单
	@Override
	public List<Orders> AdfindOrderByState(String st) throws SQLException {
		return dao.AdfindOrderByState(st);
	}
	//后台查询商品详情
	@Override
	public Orders findOrderByOid(String oid) throws SQLException {
		
		return dao.findOrderByOid(oid);
		
	}
	//更新订单状态信息
	/**
	 * 更新订单状态信息
	 */
	@Override
	public void updateOrder(Orders order) throws SQLException {
		dao.updateOrder(order);
		
	}
	

}
