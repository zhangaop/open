package com.zl.dao;

import java.sql.SQLException;
import java.util.List;

import com.zl.domain.Product;

public interface ProductDao {
	/**
	 * 查询最新商品
	 */
	List<Product> findByNews()throws SQLException;
	/**
	 * 查询最热商品
	 */
	List<Product> findByHots()throws SQLException;
	/**
	 * 通过pid查询商品信息
	 */
	Product findById(String pid)throws SQLException;
	/**
	 * 通过查询总数
	 * @param cid 
	 */
	int getCount(String cid)throws SQLException;
	//当前页显示的数据,通过cid,起始索引,每页的条数来查找
	List<Product> findByCidPage(String cid, int index, int pageSize)throws SQLException;
	//获取商品总数
	int getCountProduct()throws SQLException;
	//分页获取商品信息
	List<Product> getProduct(int i, int j)throws SQLException;
	//添加商品
	void saveProduct(Product product)throws SQLException;

}
