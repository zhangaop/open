package com.zl.dao;

import com.zl.domain.Customer;
import org.hibernate.criterion.DetachedCriteria;

import java.util.List;

public interface CustomerDao extends BaseDao<Customer> {
    /**
     * 添加客户信息
     * @param customer
     */
//    void save(Customer customer);

    /**
     * 查询客户的总数量
     * @param detachedCriteria
     * @return
     */
//    Integer count(DetachedCriteria detachedCriteria);

    /**
     *分页查询所有客户
     * @param detachedCriteria
     * @param begin
     * @param pageSize
     * @return
     */
//    List<Customer> findByPage(DetachedCriteria detachedCriteria, Integer begin, Integer pageSize);

    /**
     * 通过id查询客户信息
     * @param cust_id
     * @return
     */
//    Customer findById(Long cust_id);

    /**
     * 修改客户信息
     * @param customer
     */
//    void update(Customer customer);

    /**
     * 删除用户
     * @param customer
     */
//    void delete(Customer customer);

    /**
     * 联系人管理查询所有客户
     * @return
     */
//    List<Customer> findAll();
}
