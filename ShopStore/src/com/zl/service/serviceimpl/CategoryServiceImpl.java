package com.zl.service.serviceimpl;

import java.sql.SQLException;
import java.util.List;

import com.zl.dao.CategoryDao;
import com.zl.dao.daoimpl.CategoryDaoImpl;
import com.zl.domain.Category;
import com.zl.service.CategoryService;
import com.zl.utils.JedisUtils;

import redis.clients.jedis.Jedis;

public class CategoryServiceImpl implements CategoryService {
	/**
	 * 查询所有的商品分类
	 */
	public List<Category> findAllCategory() throws SQLException {
		CategoryDao dao = new CategoryDaoImpl();
		return dao.findAllCategory();
	}
/**
 * 后台添加商品分类
 */
	@Override
	public void addCategory(Category c) throws SQLException {
		CategoryDao dao = new CategoryDaoImpl();
		dao.addCategory(c);
			Jedis jedis = JedisUtils.getJedis();
			jedis.del("allCats");
			JedisUtils.closeJedis(jedis);
	}
	
	//删除分类
	@Override
	public void delCats(String cid) throws SQLException {
		CategoryDao dao = new CategoryDaoImpl();
		dao.delCats(cid);
		Jedis j = JedisUtils.getJedis();
		j.del("allCats");
		JedisUtils.closeJedis(j);
	}
	//获取单个货物分类信息
	@Override
	public Category findCatBycid(String cid) throws SQLException {
		CategoryDao dao = new CategoryDaoImpl();
		Category c = dao.findCatBycid(cid);
		return c;
	}
	//编辑商品分类信息
	@Override
	public void edit(Category c) throws SQLException {
		CategoryDao dao = new CategoryDaoImpl();
		dao.update(c);
		Jedis j = JedisUtils.getJedis();
		j.del("allCats");
		JedisUtils.closeJedis(j);
	}
	
}
