package com.zl.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.zl.domain.Orders;
import com.zl.domain.PageModel;
import com.zl.domain.User;

public interface OrderDao {
	/**
	 * 存储订单信息
	 * @param orders
	 * @throws SQLException
	 */
	void addOrder(Orders orders)throws SQLException;
	/**
	 * 存储单项信息
	 * @param orders
	 * @throws SQLException
	 */
	void addOrderItem(Orders orders)throws SQLException;
	/**
	 * 获取用户所有订单信息
	 * @param uid
	 * @return
	 * @throws SQLException
	 */
	List<Orders> findAllOrders(String uid)throws SQLException;
	/**
	 * 封装的是多个订单项和该订单项中的商品的信息
	 * @param oid
	 * @return
	 */
	List<Map<String, Object>> findAllOrderItemByOid(String oid)throws SQLException;
	/**
	 * 更新收货人信息
	 * @param orders
	 */
	void updateOrderAddress(Orders orders)throws SQLException;
	/**
	 * 改变支付状态state
	 * @param r6_Order
	 * @throws SQLException
	 */
	void updateOrderState(String r6_Order)throws SQLException;
	/**
	 * 获取我的订单分页信息
	 * @param user
	 * @param curNum
	 * @return
	 */
	/**
	 * 总记录数
	 * @param user
	 * @return
	 * @throws SQLException
	 */
	int gettotalRecords(User user)throws SQLException;
	/**
	 * 获取分页获取订单信息
	 * @param user
	 * @param startIndex
	 * @param pageSize
	 * @return
	 * @throws SQLException
	 */
	List<Orders> findMyOrderWithPage(User user, int startIndex, int pageSize) throws SQLException;
	/**
	 * 获取总订单信息
	 * @return
	 * @throws SQLException
	 */
	List<Orders> AdfindAllOrders()throws SQLException ;
	/**
	 * 获取不同转态的订单信息
	 * @param st
	 * @return
	 * @throws SQLException
	 */
	List<Orders> AdfindOrderByState(String st)throws SQLException;
	/**
	 * 后台查询订单详情
	 * @param oid
	 * @return
	 * @throws SQLException
	 * @throws Exception 
	 */
	Orders findOrderByOid(String oid)throws SQLException;
	/**
	 * 后台更新订单信息
	 * @param order
	 */
	void updateOrder(Orders order)throws SQLException;
	

}
