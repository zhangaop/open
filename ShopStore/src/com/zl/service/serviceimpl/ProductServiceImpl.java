package com.zl.service.serviceimpl;

import java.sql.SQLException;
import java.util.List;

import com.zl.dao.ProductDao;
import com.zl.dao.daoimpl.ProductDaoImpl;
import com.zl.domain.PageBean;
import com.zl.domain.PageModel;
import com.zl.domain.Product;
import com.zl.service.ProductService;

public class ProductServiceImpl implements ProductService {
	ProductDao dao = new ProductDaoImpl();
	//添加商品
	@Override
	public void saveProduct(Product product) throws SQLException {
		dao.saveProduct(product);
	}

	/**
	 * 分页查询所有商品
	 */
	@Override
	public PageModel findByPage(Integer curnum, int pageSize) throws SQLException {
		//获得总商品数
		int totalSize = dao.getCountProduct();
		//封装数据
		PageModel pageBean = new PageModel(curnum,totalSize,pageSize);
		//关联product集合
		List<Product> list = dao.getProduct(pageBean.getStartIndex(),pageBean.getPageSize());
		pageBean.setList(list);
		//关联url
		pageBean.setUrl("AdminProductServlet?method=findAllProductsWithPage");
		return pageBean;
	}
	
	/**
	 * 查询最新
	 */
	@Override
	public List<Product> findByNews() throws SQLException {
		
		return dao.findByNews();
	}
	/**
	 * 查询最热
	 */
	@Override
	public List<Product> findByHots() throws SQLException {
		
		return dao.findByHots();
	}
	/**
	 * 通过pid查询商品信息
	 */
	@Override
	public Product dinsById(String pid) throws SQLException {
		ProductDao dao = new ProductDaoImpl();
		return dao.findById(pid);
	}
	/**
	 * 通过cid查询分类商品,并分页
	 * @param cid
	 * @return
	 * @throws SQLException
	 */
	@Override
	public PageBean findByCid(String cid,int currentPage,int pageSize)  {
		//封装一下PageBean分页信息,返回web层
		PageBean<Product> pageBean = new  PageBean<Product>();
		//当前页
		pageBean.setCurrentPage(currentPage);
		//当前页总条数
		pageBean.setPageSize(pageSize);
		//商品总条数
		int totalSize=0;
		try {
			totalSize=dao.getCount(cid);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		pageBean.setTotalSize(totalSize);
		//总页数=总条数除以一页的条数
		int totalPage=(int) Math.ceil(1.0*totalSize/pageSize);
		pageBean.setTotalPage(totalPage);
		//当前页显示的数据,通过cid,起始索引,每页的条数来查找
		//index==(当前页-1)*每页总条数
		int index = (currentPage-1)*pageSize;
		List<Product> list;
		try {
			list = dao.findByCidPage(cid,index,pageSize);
			pageBean.setList(list);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return pageBean;
	}

}
