package com.zl.dao;

import com.zl.domain.BaseDict;

import java.util.List;

public interface BaseDictDao {
    /**
     * 获取字典数据
     * @param dict_type_code
     * @return
     */
    List<BaseDict> findByTypeCode(String dict_type_code);
}
