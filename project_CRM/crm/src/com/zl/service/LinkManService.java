package com.zl.service;

import com.zl.domain.LinkMan;
import com.zl.domain.PageBean;
import org.hibernate.criterion.DetachedCriteria;

public interface LinkManService {
    /**
     * 分页查询联系人信息
     * @param detachedCriteria
     * @param currentPage
     * @param pageSize
     * @return
     */
    PageBean<LinkMan> findAll(DetachedCriteria detachedCriteria, Integer currentPage, Integer pageSize);

    /**
     * 保存联系人信息
     * @param linkMan
     */
    void save(LinkMan linkMan);

    /**
     * 修改联系人
     * @param lkm_id
     * @return
     */
    LinkMan findById(Long lkm_id);

    /**
     * 更新修改联系人数据
     * @param linkMan
     */
    void update(LinkMan linkMan);

    /**
     * 删除联系人
     * @param linkMan
     */
    void delete(LinkMan linkMan);
}
