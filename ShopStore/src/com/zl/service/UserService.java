package com.zl.service;

import java.sql.SQLException;

import com.zl.domain.User;

public interface UserService {
	//用户注册
	void UserRegist(User user)throws SQLException;
	//发送邮件验证
	User findByCode(String code) throws SQLException;
	//激活成功则更新用户状态
	void update(User user)throws SQLException;
	//用户登录验证
	User login(String username, String password)throws SQLException;
	//Ajax异步校验用户名
	User findByUsername(String username)throws SQLException;
}
